﻿using System;

class AddressExLoROM
{
    public static int PcToSnes(int PcAddress, out bool IsValidPcAddress)
    {
        int SnesAddress = 0;

        if (PcAddress >= 0x7F0000)
        {
            IsValidPcAddress = false;
        }
        else
        {
            IsValidPcAddress = true;
            SnesAddress = PcAddress << 1;
            SnesAddress &= 0x7F0000;
            SnesAddress |= (PcAddress | 0x8000) & 0xFFFF;

            if (PcAddress < 0x400000)
            {
                SnesAddress += 0x800000;
            }
        }
        return SnesAddress;
    }

    public static int SnesToPc(int SnesAddress, out bool IsValidSnesAddress)
    {
        int PcAddress = 0;

        if ((SnesAddress >= 0x808000 && SnesAddress <= 0xFFFFFF) || (SnesAddress >= 0x008000 && SnesAddress <= 0x7dffff))
        {
            IsValidSnesAddress = true;

            PcAddress = (SnesAddress & 0x7FFF | ((SnesAddress & 0x7F0000) >> 1));

            if (SnesAddress < 0x800000)
            {
                PcAddress += 0x400000;
            }
        }
        else
        {
            IsValidSnesAddress = false;
        }
        return PcAddress;
    }
}