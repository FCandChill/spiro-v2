﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

/// <summary>
/// Transforms text to SquishyText characters.
/// </summary>
public class SquishyText
{
    private string FileName { get; set; }
    private bool UseRegex { get; set; }

    private List<SquishyTextEntry> l_SquishyText { get; set; }

    public SquishyText(string FileName, bool UseRegex)
    {
        if (string.IsNullOrEmpty(FileName))
        {
            throw new Exception();
        }
        this.FileName = FileName;
        this.UseRegex = UseRegex;
        l_SquishyText = new List<SquishyTextEntry>();
    }

    public void ReadSquishyTextFile()
    {


        using (StreamReader sr = new StreamReader(FileName))
        {
            string line = "";

            while (!sr.EndOfStream && (line = sr.ReadLine()).Contains(Constants.DICTIONARY_SPLIT))
            {
                if (!line.StartsWith("//"))
                {
                    string[] a_SquishyText = line.Split(Constants.DICTIONARY_SPLIT);
                    l_SquishyText.Add(new SquishyTextEntry(a_SquishyText[0], a_SquishyText[1]));
                }
            }
        }
    }

    public string AddSquishyText(string DialogueEntry)
    {
        string NewText = "";

        if (l_SquishyText.Count != 0)
        {
            const string PATTERN = @"(\(.*?\))|(\[.*?\])|({.*?})";
            const string EXCLUDE = @"(\(.*?\))|(\[.*?\])|({.*?})";

            string[] a_chunks = Regex.Split(DialogueEntry, PATTERN).Where(s => s != String.Empty).ToArray();

            foreach (string s in a_chunks)
            {
                string Insert = s;
                if (UseRegex || !Regex.IsMatch(s, EXCLUDE))
                {
                    foreach(SquishyTextEntry entry in l_SquishyText)
                    {
                        if (UseRegex)
                        {
                            Insert = Regex.Replace(Insert, entry.Find, entry.Replace);
                        } 
                        else
                        {
                            Insert = Insert.Replace(entry.Find, entry.Replace);
                        }
                    }
                }

                NewText = string.Concat(NewText, Insert);
            }
        }
        else
        {
            NewText = DialogueEntry;
        }

        return NewText;
    }
}

public struct SquishyTextEntry
{
    public string Find { private set; get; }
    public string Replace { private set; get; }

    public SquishyTextEntry(string Find, string Replace)
    {
        this.Find = Find;
        this.Replace = Replace;
    }
}