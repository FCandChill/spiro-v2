﻿/* 
 * Class        :   CharacterTable.cs
 * Author       :   FCandChill
 * Consultant   :   Kaijitani-Eizan
 * Description  :   
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

/// <summary>
/// Stores the width of each character. For auto-line breaking.
/// 
/// A lot of logic is borrowed from TextParsing class.
/// </summary>
class WidthCalc
{
    private string Filename { get; set; }

    /// <summary>
    /// Key     = CHR hex value
    /// Value   = length
    /// </summary>
    private SortedDictionary<byte[], int> CHRLengthlookup { get; set; }
    private SortedDictionary<byte[], int> ControlCodeLengthlookup { get; set; }

    private Queue<byte[]> DictionaryKeys { get; set; }

    /*
     * It was pointed out that it's weird that the constructor is empty.
     * The reason why it's setup this way is so you can swap CHR files
     * easily.
     */
    public WidthCalc() { }

    public void LoadCHR(string FilenamePart)
    {
        Filename = string.Format(Constants.Path_To_x_length, FilenamePart);
        CHRLengthlookup = new SortedDictionary<byte[], int>(new ByteComparer());

        if (!File.Exists(Filename))
        {
            throw new Exception($"File doesn't exist: {Filename}");
        }

        using (FileStream inputOpenedFile = File.Open(Filename, FileMode.Open))
        {
            using (StreamReader sr = new StreamReader(inputOpenedFile, Encoding.UTF8))
            {
                foreach (string fileLine in sr.ReadToEnd().Split(Constants.txt_newline.ToCharArray(), StringSplitOptions.RemoveEmptyEntries))
                {
                    byte[] HexValue = GetHexValueFromFileLine(fileLine);
                    int Length = ParseLengthFileLine(fileLine);
                    CHRLengthlookup.Add(HexValue, Length);
                }
            }
        }
    }

    public void LoadDictionaryTemplate(string Filename)
    {
        Filename = MyDictionary.GenerateFilename(false, Filename);

        DictionaryKeys = new Queue<byte[]>();
        ControlCodeLengthlookup = new SortedDictionary<byte[], int>(new ByteComparer());

        using (FileStream inputOpenedFile = File.Open(Filename, FileMode.Open))
        {
            using (StreamReader sr = new StreamReader(inputOpenedFile, Encoding.UTF8))
            {
                foreach (string fileLine in sr.ReadToEnd().Split(Constants.txt_newline.ToCharArray(), StringSplitOptions.RemoveEmptyEntries))
                {
                    if (!fileLine.StartsWith("//"))
                    {
                        byte[] HexValue = GetHexValueFromFileLine(fileLine);
                        DictionaryKeys.Enqueue(HexValue);
                    }
                }
            }
        }
    }

    public string TokimekiMihoTransAddLineBreaks(int entryNumber, string dialogueEntry, int pixelsPerLine, MyDictionary chara, bool autoEdgeLinebreak)
    {
        //Split the dialogue so my control codes are in one index while the dialogue is 
        string[] spiroControlCode = dialogueEntry.Split(new char[] { '\\', '/' }, StringSplitOptions.RemoveEmptyEntries);
        dialogueEntry = Regex.Replace(dialogueEntry, @"\\.*?/", "");

        /*
         * Parse my own control codes.
         * LineWidth = Skips the linebreaking code entirely
         * LineBreakSkip = let's you set the width of the textbox
         */

        bool overrideLineSkip = false;
        bool lineAfterwards = false;
        string newScriptEntry = "";

        if (spiroControlCode.Length > 1)
        {
            foreach (string s in spiroControlCode)
            {
                if (s.StartsWith("LineWidth"))
                {
                    pixelsPerLine = int.Parse(s.Split('=')[1]);
                }
                else if (s.Equals("LineBreakSkip"))
                {
                    newScriptEntry = dialogueEntry;
                    overrideLineSkip = true;
                    break;
                }
                else if (s.Equals("LineAfterwards"))
                {
                    lineAfterwards = true;
                    break;
                }
            }
        }

        if (!overrideLineSkip)
        {
            /*
             * Split the dialogue string into chunks that we can work with.
             * Space get their own entries.
             * 
             * Control codes have their own index
             * Bytes get their own index
             * And words like "Cat" get their own index. 
             * Characters like [...] are part of a word. So you'll 
             * get indexes that have "Hmm[...].
             */
            string[] a_chunks = (new Regex(@"(\(.*?\))|(\s+(?![^\(]*\))(?![^\[]*\]))|({.*?})|(\<.*?\>)|(\[LINE\])|(\[NEXT\])|(\[STOP\])")).Split(dialogueEntry).Where(s => s != String.Empty).ToArray();

            int[] ChunkLength = new int[a_chunks.Length];

            for (int i = 0; i < a_chunks.Length; i++)
            {
                string ChunkToInsert = a_chunks[i];

                if (ChunkToInsert.Contains("Lame!"))
                {

                }

                if (ChunkToInsert[0] == Constants.ACTION1)
                {
                    //determine how much space a controlcode takes up.
                    ChunkLength[i] = 0;
                }
                else if (ChunkToInsert[0] == '<')
                {
                    ChunkLength[i] += 0;
                }
                else if (ChunkToInsert[0] != Constants.BYTE1)
                {
                    /*
                     * Have each index in SubChunk contains a single character.
                     * Stuff in brackets count as a single character. 
                     * 
                     * For example:
                     * 
                     * Full word: "Hmm[...]"
                     * 
                     * Becomes:
                     * 
                     * [0]=H
                     * [1]=m
                     * [2]=m
                     * [3]=[...]
                     * 
                     * This makes the code simple. 
                     */
                    string[] a_SubChunk = (new Regex(@"(\[.*?\])|(.)")).Split(ChunkToInsert).Where(s => s != string.Empty).ToArray();

                    foreach (string CharacterToGetWidthOf in a_SubChunk)
                    {
                        if (chara.GetCHRByteValue(CharacterToGetWidthOf, out byte[] hexid))
                        {
                            if (CHRLengthlookup.ContainsKey(hexid))
                            {
                                ChunkLength[i] += CHRLengthlookup[hexid];
                            }
                            else
                            {

                                throw new Exception($"Length calc lookup key does not exist: {MyMath.DecToHex(hexid)}.");
                            }
                        }
                        else
                        {
                            throw new Exception($"Entry no #{entryNumber}. Unable to insert the following string: \"{CharacterToGetWidthOf}\".");
                        }
                    }
                }
            }

            chara.GetCHRByteValue(Constants.SPACE, out byte[] spaceId);
            int WidthOfSpace = CHRLengthlookup[spaceId];

            chara.GetCHRByteValue(Constants.SPACE, out byte[] lineBreakId);
            int WidthOfLineBreak = CHRLengthlookup[lineBreakId];

            int originalPixelsPerLine = pixelsPerLine;
            bool willLineBreak = false;
            int lineLength = 0;
            bool delayedSpaceInsertion = false;

            int maxLineNo = 5;
            int currentLine = 1;

            bool newTextboxArea = false;
            bool previousEdgeCase = false;

            void PadOutLine()
            {
                int NumberOfNewlinesToAdd = maxLineNo - currentLine;

                for (int k = 0; k < NumberOfNewlinesToAdd; k++)
                {
                    newScriptEntry = string.Concat(newScriptEntry, "[LINE]");
                }
                currentLine = 1;
            }

            //Match on control codes and byte data.
            Regex regex = new Regex(@"(\(.*?\))|({.*?})");

            for (int i = 0; i < a_chunks.Length; i++)
            {
                void IncrementCurrentLine()
                {
                    currentLine++;
                    if (currentLine == maxLineNo)
                    {
                        currentLine = 1;
                    }

                    if (currentLine == 4)
                    {
                        pixelsPerLine = originalPixelsPerLine - 8; //to adjust for cursor
                    }
                    else
                    {
                        pixelsPerLine = originalPixelsPerLine;
                    }
                }

                string chunkToInsert = a_chunks[i];

                if (newScriptEntry.Contains("Lame!"))
                {

                }

                if (chunkToInsert == "[STOP]" && lineAfterwards)
                {
                    PadOutLine();
                }

                if (chunkToInsert == "[LINE]")
                {
                    if (previousEdgeCase)
                    {
                        previousEdgeCase = false;
                        continue;
                    }

                    willLineBreak = true;
                    delayedSpaceInsertion = false;
                }
                else if (chunkToInsert == "[NEXT]")
                {
                    lineLength = 0;
                }
                else if (chunkToInsert == "(PAGE)")
                {
                    lineLength = 0;
                    currentLine = 0;
                    pixelsPerLine = originalPixelsPerLine;
                }

                previousEdgeCase = false;

                bool edgeCase = lineLength + ChunkLength[i] == pixelsPerLine && !newTextboxArea && autoEdgeLinebreak;

                if ((lineLength + ChunkLength[i] > pixelsPerLine) || edgeCase)
                {
                    willLineBreak = true;
                }

                //Remove a space at the end of a line if we're linebreaking.
                if (willLineBreak && !edgeCase && i > 0 && a_chunks[i - 1] == Constants.SPACE)
                {
                    newScriptEntry = newScriptEntry.Substring(0, newScriptEntry.Length - 1);
                    lineLength -= WidthOfSpace;
                }

                if (!willLineBreak && autoEdgeLinebreak && chunkToInsert != Constants.SPACE && edgeCase)
                {
                    willLineBreak = true;
                }

                if (willLineBreak)
                {
                    if (!(edgeCase))
                    {
                        if (newScriptEntry.EndsWith(Constants.SPACE) && WidthOfLineBreak == 0)
                        {
                            newScriptEntry = newScriptEntry.TrimEnd();
                            lineLength -= WidthOfSpace;
                        }
                    }

                    /*
                     * Don't add a linebreak if there's nothing ahead but control codes at the end of the speaker's line.
                     */
                    int j = i - 1;
                    while (++j < a_chunks.Length && regex.Match(a_chunks[j]).Success && a_chunks[j] != "[LINE]") { }

                    //Game already autolinebreaks in right on the edge.
                    if (autoEdgeLinebreak && edgeCase)
                    {
                        willLineBreak = false;
                        lineLength = -ChunkLength[i];
                        IncrementCurrentLine();
                        previousEdgeCase = true;
                    }
                    else if (j < a_chunks.Length)
                    {
                        if (!newTextboxArea)
                        {
                            newScriptEntry = string.Concat(newScriptEntry, "[LINE]");
                            lineLength += WidthOfLineBreak;
                            IncrementCurrentLine();
                        }
                        else
                        {
                            PadOutLine();
                            newTextboxArea = false;
                        }

                        lineLength = 0;
                    }

                    willLineBreak = false;
                }

                if (chunkToInsert != "[LINE]")
                {
                    /*
                     * Delaying the space insertion makes it easier to determine when
                     * we have a trailing space at the end of a line.
                     * 
                     * For example, this is easier to determine that
                     * we need to remove an extra space:
                     * "(CONTROL CODE) "
                     * 
                     * This is not:
                     * " (CONTROL CODE)"
                     */

                    if (chunkToInsert == Constants.SPACE && i + 1 < ChunkLength.Length && regex.Match(a_chunks[i + 1]).Success)
                    {
                        delayedSpaceInsertion = true;
                    }
                    else
                    {
                        //Not sure why there's two white space removers (one above), but I'll leave this alone now.
                        if (!(lineLength == 0 && chunkToInsert == Constants.SPACE))
                        {
                            newScriptEntry = string.Concat(newScriptEntry, chunkToInsert);
                            lineLength += ChunkLength[i];
                        }

                        if (delayedSpaceInsertion && i + 1 < ChunkLength.Length && !regex.Match(a_chunks[i + 1]).Success)
                        {
                            newScriptEntry = string.Concat(newScriptEntry, Constants.SPACE);
                            lineLength += WidthOfSpace;
                            delayedSpaceInsertion = false;
                        }
                    }
                }
            }
        }

        if (newScriptEntry.Contains('<') || newScriptEntry.Contains('>'))
        {
            throw new Exception("Script cannot contain < or > after autolinebreaking. Something's off.");
        }

        return newScriptEntry;
    }

    public string MetalSladerTransAddLineBreaks(int EntryNumber, string DialogueEntry, int PixelsPerLine, MyDictionary chara, bool InGameAutotLineBreak = false)
    {
        //Split the dialogue so my control codes are in one index while the dialogue is 
        string[] SpiroControlCode = DialogueEntry.Split(new char[] { '\\', '/' }, StringSplitOptions.RemoveEmptyEntries);
        DialogueEntry = Regex.Replace(DialogueEntry, @"\\.*?/", "");

        /*
         * Parse my own control codes.
         * LineWidth = Skips the linebreaking code entirely
         * LineBreakSkip = let's you set the width of the textbox
         */

        if (EntryNumber == 2227)
        {

        }

        bool OverrideLineSkip = false;
        string NewScriptEntry = "";
        bool portraitAlreadyOpen = false;

        if (SpiroControlCode.Length > 1)
        {
            foreach (string s in SpiroControlCode)
            {
                if (s.StartsWith("LineWidth="))
                {
                    PixelsPerLine = int.Parse(s.Split('=')[1]);
                }
                else if (s.Equals("LineBreakSkip"))
                {
                    NewScriptEntry = DialogueEntry;
                    OverrideLineSkip = true;
                    break;
                }
                else if (s.Equals("LineWidthPortraitShowing"))
                {
                    portraitAlreadyOpen = true;
                    break;
                }
            }
        }

        if (!OverrideLineSkip)
        {
            const string
                CODE_PORTRAIT_OPEN = "(CODE 10 Portrait)",
                CODE_PORTRAIT_CLOSE = "(CODE 11)",
                CODE_ENDQUOTE = "(End quote)",
                CODE_WAIT = "(WAIT)",
                CODE_NAMECARD_COLON = ":";

            const int LENGTH_OF_CURSOR = 8;
            const int LENGTH_OF_PORTRAINT = 64;

            /*
             * Split the dialogue string into chunks that we can work with.
             * Space get their own entries.
             * 
             * Control codes have their own index
             * Bytes get their own index
             * And words like "Cat" get their own index. 
             * Characters like [...] are part of a word. So you'll 
             * get indexes that have "Hmm[...].
             */
            string[] a_chunks;
            if (InGameAutotLineBreak)
            {
                a_chunks = (new Regex(@"(\((?!End quote).*?\))|(\s+(?![^\(]*\))(?![^\[]*\]))|({.*?})")).Split(DialogueEntry).Where(s => s != String.Empty).ToArray();
            }
            else
            {
                a_chunks = (new Regex(@"(\(.*?\))|(\s+(?![^\(]*\))(?![^\[]*\]))|({.*?})")).Split(DialogueEntry).Where(s => s != String.Empty).ToArray();
            }

            int[] a_chunkLength = new int[a_chunks.Length];

            for (int i = 0; i < a_chunks.Length; i++)
            {
                string ChunkToInsert = a_chunks[i];

                if (ChunkToInsert[0] == Constants.ACTION1)
                {
                    if (!chara.DictSortedByString.ContainsKey(ChunkToInsert))
                    {
                        throw new Exception($"The following is not a valid control code: {ChunkToInsert}.");
                    }

                    //determine how much space a controlcode takes up.
                    byte[] id = chara.DictSortedByString[ChunkToInsert].ByKey.ElementAt(0).Key;

                    if (!ControlCodeLengthlookup.ContainsKey(id))
                    {
                        throw new Exception($"The following id not in the control code length lookup: {MyMath.FormatByteInBrackets(id)}");
                    }

                    a_chunkLength[i] = ControlCodeLengthlookup[id];
                }
                else if (ChunkToInsert[0] != Constants.BYTE1)
                {
                    /*
                     * Have each index in SubChunk contains a single character.
                     * Stuff in brackets count as a single character. 
                     * 
                     * For example:
                     * 
                     * Full word: "Hmm[...]"
                     * 
                     * Becomes:
                     * 
                     * [0]=H
                     * [1]=m
                     * [2]=m
                     * [3]=[...]
                     * 
                     * This makes the code simple. 
                     */
                    string[] a_SubChunk = (new Regex(@"(\(.*?\))|(\[.*?\])|(.)")).Split(ChunkToInsert).Where(s => s != string.Empty).ToArray();

                    foreach (string CharacterToGetWidthOf in a_SubChunk)
                    {
                        if (CharacterToGetWidthOf[0] == Constants.ACTION1)
                        {
                            if (!chara.DictSortedByString.ContainsKey(CharacterToGetWidthOf))
                            {
                                throw new Exception($"The following is not a valid control code: {CharacterToGetWidthOf}.");
                            }

                            //determine how much space a controlcode takes up.
                            a_chunkLength[i] += ControlCodeLengthlookup[chara.DictSortedByString[CharacterToGetWidthOf].ByKey.ElementAt(0).Key];
                        }
                        else if (chara.GetCHRByteValue(CharacterToGetWidthOf, out byte[] hexid))
                        {
                            a_chunkLength[i] += CHRLengthlookup[hexid];
                        }
                        else
                        {
                            throw new Exception($"Entry no #{EntryNumber}. Unable to insert the following string: \"{CharacterToGetWidthOf}\".");
                        }
                    }
                }
            }


            chara.GetCHRByteValue(Constants.SPACE, out byte[] space_id);
            int WidthOfSpace = CHRLengthlookup[space_id];

            int CurrentLine = 1;
            int OriginalPixelsPerLine = PixelsPerLine;
            bool WillLineBreak = false;
            int LineLength = 0;
            bool DelayedSpaceInsertion = false;
            bool WaitFlag = false;
            bool PortraitDisplay = false;
            bool AdjustForEndquote = false;
            bool LastWordChunk = false;
            int LastChunkLength = 0;

            //Match on control codes and byte data.

            Regex RegexSplit;

            if (InGameAutotLineBreak)
            {
                RegexSplit = new Regex(@"(\((?!End quote).*?\))|({.*?})");
            }
            else
            {
                RegexSplit = new Regex(@"(\(.*?\))|({.*?})");
            }

            for (int i = 0; i < a_chunks.Length; i++)
            {
                if (portraitAlreadyOpen)
                {
                    PortraitDisplay = true;
                    PixelsPerLine = GetLengthOfTextbox();
                    portraitAlreadyOpen = false;
                }

                int GetLengthOfTextbox()
                {
                    int TextboxLength = OriginalPixelsPerLine;

                    /*
                     * The wait cursor decrement doesn't stack with the portrait 
                     * length decrement.
                     * 
                     * You see, you can't actually write in the column where the cursor
                     * would be when a portrait is displayed.
                     */
                    if (PortraitDisplay)
                    {
                        TextboxLength = OriginalPixelsPerLine - LENGTH_OF_PORTRAINT;
                    }
                    else if (WaitFlag)
                    {
                        TextboxLength = OriginalPixelsPerLine - LENGTH_OF_CURSOR;
                    }

                    if (AdjustForEndquote)
                    {
                        TextboxLength -= 8;
                    }


                    return TextboxLength;
                }

                string ChunkToInsert = a_chunks[i];

                if (InGameAutotLineBreak && (LineLength == 0 && i != 0 && (ChunkToInsert == Constants.SPACE || ChunkToInsert == Constants.NEWLINE)))
                {
                    continue;
                }

                LastWordChunk = false;
                LastChunkLength = 0;

                /*
                 * Did we process the wait control code?
                 * If we did, set the textbox width back,
                 * we are no longer factoring in the "wait cursor"
                 */
                if (WaitFlag)
                {
                    WaitFlag = false;
                    PixelsPerLine = GetLengthOfTextbox();
                }

                if (AdjustForEndquote)
                {
                    AdjustForEndquote = false;
                    PixelsPerLine = GetLengthOfTextbox();
                }

                if (!WaitFlag && CurrentLine > 3)
                {
                    //Look ahead and see if there's a (WAIT) control code ahead.
                    int j = i;
                    while (++j < a_chunks.Length && RegexSplit.Match(a_chunks[j]).Success && a_chunks[j] != CODE_WAIT && !a_chunks[j].Contains(CODE_NAMECARD_COLON) && a_chunks[j] != Constants.NEWLINE) { }

                    if (j < a_chunks.Length && a_chunks[j] == CODE_WAIT)
                    {
                        //Reduce the max number of pixels per line by the arrows width.
                        //Otherwise the text would overlap with the next arrow.
                        WaitFlag = true;
                        PixelsPerLine = GetLengthOfTextbox();
                    }
                }

                /*
                 * Do we have the have's own autolinebreaking code enabled.
                 * (The code sucks and actually restricts how 
                 * much text you can have onscreen)
                 */

                if (InGameAutotLineBreak)
                {
                    if (ChunkToInsert.Contains(CODE_ENDQUOTE))
                    {
                        AdjustForEndquote = true;
                        PixelsPerLine = GetLengthOfTextbox();
                    }
                    else if (!ChunkToInsert.Contains(Constants.SPACE))
                    {
                        int j = i;
                        while (++j < a_chunks.Length && RegexSplit.Match(a_chunks[j]).Success && !a_chunks[j].Contains(CODE_ENDQUOTE) && !a_chunks[j].Contains(CODE_NAMECARD_COLON) && a_chunks[j] != Constants.NEWLINE) { }


                        if (j < a_chunks.Length && a_chunks[j].Contains(CODE_ENDQUOTE))
                        {
                            AdjustForEndquote = true;
                            PixelsPerLine = GetLengthOfTextbox();
                        }
                    }
                }

                if (ChunkToInsert[0] == Constants.ACTION1)
                {
                    if (ChunkToInsert.Contains(CODE_NAMECARD_COLON) || ChunkToInsert.Contains(CODE_ENDQUOTE))
                    {
                        LineLength = 0;
                        DelayedSpaceInsertion = false;
                    }

                    if (ChunkToInsert.Contains(CODE_ENDQUOTE))
                    {
                        CurrentLine++;
                    }

                    if (ChunkToInsert == Constants.NEWLINE)
                    {
                        LineLength = 0;
                        CurrentLine++;
                        DelayedSpaceInsertion = false;
                    }

                    if (ChunkToInsert == CODE_PORTRAIT_OPEN)
                    {
                        PortraitDisplay = true;
                        PixelsPerLine = GetLengthOfTextbox();
                        CurrentLine = 1;
                    }
                    else if (ChunkToInsert == CODE_PORTRAIT_CLOSE)
                    {
                        PortraitDisplay = false;
                        PixelsPerLine = GetLengthOfTextbox();
                        CurrentLine = 1;
                    }
                }

                /*
                 * Check if a word is split by a control code.
                 * 
                 * If it is, see if we need to linebreak.
                 */
                if (!WillLineBreak && ChunkToInsert != Constants.SPACE && ChunkToInsert[0] != Constants.ACTION1 && ChunkToInsert[0] != Constants.BYTE1)
                {
                    int j = i - 1;

                    int LengthOfFullWord = 0;
                    while (++j < a_chunks.Length && a_chunks[j] != Constants.SPACE && !a_chunks[j].Contains(CODE_NAMECARD_COLON) && a_chunks[j] != Constants.NEWLINE)
                    {
                        LengthOfFullWord += a_chunkLength[j];
                    }

                    if (j == a_chunks.Length)
                    {
                        LastWordChunk = true;
                        LastChunkLength = LengthOfFullWord;
                    }


                    if (LengthOfFullWord > a_chunkLength[i])
                    {
                        if (LineLength + LengthOfFullWord > PixelsPerLine)
                        {
                            LineLength = 0;
                            WillLineBreak = true;
                        }
                    }
                }

                if (!WillLineBreak)
                {
                    int u = i;

                    if (!RegexSplit.Match(a_chunks[u]).Success)
                    {
                        //Check to see if we didn't already identify 
                        //it's the last chunk previously in function (when we were checking for a word 
                        //split by control code(s).
                        if (!LastWordChunk)
                        {
                            LastChunkLength += a_chunkLength[u];

                            while (++u < a_chunks.Length && RegexSplit.Match(a_chunks[u]).Success && a_chunks[u] != Constants.SPACE && a_chunks[u] != Constants.NEWLINE)
                            {
                                LastChunkLength += a_chunkLength[u];
                            }

                            if (u == a_chunks.Length)
                            {
                                LastWordChunk = true;
                            }
                        }
                    }

                    bool NormalLineBreak = (LineLength + a_chunkLength[i] > PixelsPerLine);

                    //Kind of a bad way to check if this is the final chunk with
                    //text to insert.
                    if (!NormalLineBreak && (CurrentLine >= 3 && !WaitFlag && LastWordChunk && !PortraitDisplay))
                    {
                        if (LineLength + LastChunkLength > PixelsPerLine - LENGTH_OF_CURSOR)
                        {
                            if (Global.Debug)
                            {
                                Console.WriteLine($"Autolinebreak: end of line; Entryno: {EntryNumber}.");
                            }
                            WillLineBreak = true;
                        }
                    }
                    else
                    {
                        if (NormalLineBreak)
                        {
                            WillLineBreak = true;
                        }
                    }
                }

                //Remove a space at the end of a line if we're linebreaking.
                if (WillLineBreak && NewScriptEntry.Length > 0 && NewScriptEntry[NewScriptEntry.Length - 1].ToString() == Constants.SPACE)
                {
                    int j = i;
                    while (++j < a_chunks.Length && RegexSplit.Match(a_chunks[j]).Success && a_chunks[j] != Constants.NEWLINE) { }

                    if (j < a_chunks.Length && a_chunks[j] == Constants.NEWLINE)
                    {
                        NewScriptEntry = NewScriptEntry.Substring(0, NewScriptEntry.Length - 1);
                        LineLength -= WidthOfSpace;
                    }
                }

                if (WillLineBreak)
                {
                    if (NewScriptEntry.EndsWith(Constants.SPACE))
                    {
                        NewScriptEntry = NewScriptEntry.TrimEnd();
                    }

                    /*
                     * Don't add a linebreak if there's nothing ahead but control codes at the end of the speaker's line.
                     */
                    int j = i - 1;
                    while (++j < a_chunks.Length && RegexSplit.Match(a_chunks[j]).Success && !a_chunks[j].Contains(CODE_NAMECARD_COLON) && a_chunks[j] != Constants.NEWLINE && a_chunks[j].Contains(CODE_ENDQUOTE)) { }

                    if (j < a_chunks.Length && !a_chunks[j].Contains(CODE_NAMECARD_COLON))
                    {
                        NewScriptEntry = string.Concat(NewScriptEntry, Constants.NEWLINE);
                        LineLength = 0;
                        CurrentLine++;
                    }

                    WillLineBreak = false;
                }

                if (InGameAutotLineBreak && (LineLength + a_chunkLength[i] == PixelsPerLine))
                {
                    LineLength = 0 - a_chunkLength[i];
                }

                if (!(LineLength == 0 && ChunkToInsert == Constants.SPACE))
                {
                    /*
                     * Delaying the space insertion makes it easier to determine when
                     * we have a trailing space at the end of a line.
                     * 
                     * For example, this is easier to determine that
                     * we need to remove an extra space:
                     * "(CONTROL CODE) "
                     * 
                     * This is not:
                     * " (CONTROL CODE)"
                     */

                    if (ChunkToInsert == Constants.SPACE && i + 1 < a_chunkLength.Length && RegexSplit.Match(a_chunks[i + 1]).Success)
                    {
                        DelayedSpaceInsertion = true;
                    }
                    else
                    {
                        NewScriptEntry = string.Concat(NewScriptEntry, ChunkToInsert);
                        LineLength += a_chunkLength[i];

                        if (DelayedSpaceInsertion && i + 1 < a_chunkLength.Length && !RegexSplit.Match(a_chunks[i + 1]).Success)
                        {
                            NewScriptEntry = string.Concat(NewScriptEntry, Constants.SPACE);
                            LineLength += WidthOfSpace;
                            DelayedSpaceInsertion = false;
                        }

                        if (ChunkToInsert.Contains(CODE_ENDQUOTE))
                        {
                            LineLength = 0;
                            DelayedSpaceInsertion = false;
                        }
                    }
                }
            }
        }

        return NewScriptEntry;
    }

    /// <summary>
    /// Gets the pixel length for dictionary values. Useful only for control codes that have text behind them.
    /// 
    /// It writes non-control code entries as well. So some clockcycle are wasted.
    /// 
    /// Keep in mind, the text in the control codes can't be broken up between lines.
    /// </summary>
    /// <param name="Line"></param>
    /// <param name="Chara"></param>
    public void AddDictionaryEntryLength(string Line, MyDictionary Chara)
    {
        int LengthOfDictionary = 0;
        Line = Regex.Replace(Line, "{.*?}", "");

        for (int i = 0; i < Line.Length; i++)
        {
            string Character;
            int length = 0;
            byte[] possibleKey;
            do
            {
                if (++length > Line.Length - i)
                {
                    throw new Exception();
                }

                Character = Line.Substring(i, length);
            }
            while (!Chara.GetCHRByteValue(Character, out possibleKey));

            if (!CHRLengthlookup.ContainsKey(possibleKey))
            {
                throw new Exception($"CHR key not in length list: $#{MyMath.GetValueOfBytes(possibleKey):X2}. Make sure the value is in the length file.");
            }

            LengthOfDictionary += CHRLengthlookup[possibleKey];
            i += Character.Length - 1;
        }

        ControlCodeLengthlookup.Add(DictionaryKeys.Dequeue(), LengthOfDictionary);
    }

    //From the dictionary class more or less.
    private byte[] GetHexValueFromFileLine(string Str)
    {
        int Index = Str.IndexOf(Constants.SEPERATOR);
        return MyMath.HexToBytes(Str.Substring(0, Index));
    }

    /// <summary>
    /// The argument a line from the length file.
    /// This file stores the lengths of each 
    /// </summary>
    /// <param name="str"></param>
    /// <returns></returns>
    private int ParseLengthFileLine(string str)
    {
        int index = str.IndexOf(Constants.SEPERATOR);
        return int.Parse(str.Substring(index + Constants.SEPERATOR.Length).ToString());
    }
}
