﻿
public static class Constants
{
    public const int VERSION = 2;

    public const byte FREESPACE_byte = 0xFF;
    public const char
        MULTI1 = '[',
        MULTI2 = ']',
        ACTION1 = '(',
        ACTION2 = ')',
        BYTE1 = '{',
        BYTE2 = '}',
        CODE_LENGTH1 = '<',
        CODE_LENGTH2 = '>',

        SPLIT_ADDRESS = ',',

        DICTIONARY_SPLIT = '=';

    public const int DefaultPointerIndex = 0;
    public static readonly string
        SEPERATOR = "=",

        txt_newline = "\r\n",
        EndEntry = ACTION1 + "STOP" + ACTION2,
        NEWLINE = ACTION1 + "LINE" + ACTION2,

        REGEX_SPLIT_STRING = @"(\[.*?\])|(\<.*?\>)|(\{.*?\})|(\(.*?\))|(.)";



    public const string
        ROOT = "Root",
        NUMBER = "no",
        SCRIPTENTRY = "ScriptEntry",

        SPACE = " ";

    public const string
        Path_To_x_both = @"{0}.tbl",
        Path_To_x_orig = @"{0} - Original.tbl",
        Path_To_dict_template = @"{0}_template.tbl",
        Path_To_x_new = @"{0} - New.tbl",
        Path_To_x_length = @"{0} - Length.tbl";


    public const int
        CODE_ARGUMENT_LENGTH_NULL = 0,

        GENERIC_NULL = -1;
}
