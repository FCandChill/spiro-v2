﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using static Enums;

public class CHR
{
    public int BiggestCHRKeyArraySize { get; set; }
    public SortedDictionary<byte[], string> ByKey;
    public SortedDictionary<string, byte[]> ByString;
    public SortedDictionary<string, DictEntry> DictSortedByString { get; set; }
}

public class Dictionaries
{
    public int UnsortedIndex { get; set; }
    public int BiggestDictionaryKeyArraySize { get; set; }
    public SortedDictionary<byte[], byte[]> DictSortByKey { get; set; }
    public Dictionary<byte[], byte[]> DictUnsortByKey { get; set; }
    public SortedDictionary<byte[], ControlCodeEntry> DictControlcode { get; set; }
    public SortedDictionary<string, DictEntry> DictSortedByString { get; set; }
}

public class DictEntry
{
    public Dictionary<byte[], CharEntry> ByKey;
    public Dictionary<string, byte[]> ByString;
    public int MaxNumberOfCharacters { get; set; }
    public DictEntry()
    {
        ByKey = new Dictionary<byte[], CharEntry>();
        ByString = new Dictionary<string, byte[]>();
        MaxNumberOfCharacters = 0;
    }
}

public class CharEntry
{
    [DebuggerDisplay("{StringVal}")]
    public byte[] ByteVal { get; set; }

    public string StringVal { get; set; }

    public CharEntry(byte[] ByteVal, string StringVal)
    {
        this.ByteVal = ByteVal;
        this.StringVal = StringVal;
    }
}

public class ControlCodeEntry
{
    public string StringRepresentation { get; set; }
    public int CodeArgumentLength { get; set; }

    public ControlCodeEntry(string StringRepresentation, int CodeArgumentLength)
    {
        this.StringRepresentation = StringRepresentation;
        this.CodeArgumentLength = CodeArgumentLength;
    }
}

public class MyDictionary
{
    private string CurrentCHR { get; set; }
    private string CurrentDictionary { get; set; }

    public int BiggestCHRKeyArraySize
    {
        get => CHRs[CurrentCHR].BiggestCHRKeyArraySize;
        private set => CHRs[CurrentCHR].BiggestCHRKeyArraySize = value;
    }

    public int BiggestDictionaryKeyArraySize
    {
        get
        {
            // It's possible for a dictionary to not be loaded if we're operating directly from a table file.
            // If a table file isn't loaded, return 0
            if (Dictionaries.ContainsKey((CurrentCHR, CurrentDictionary)))
            {
                return Dictionaries[(CurrentCHR, CurrentDictionary)].BiggestDictionaryKeyArraySize;
            }
            else
            {
                return 0;
            }
        }
        private set => Dictionaries[(CurrentCHR, CurrentDictionary)].BiggestDictionaryKeyArraySize = value;
    }
    public SortedDictionary<byte[], ControlCodeEntry> DictControlcode
    {
        get => Dictionaries[(CurrentCHR, CurrentDictionary)].DictControlcode;
        set => Dictionaries[(CurrentCHR, CurrentDictionary)].DictControlcode = value;
    }

    public SortedDictionary<string, DictEntry> DictSortedByString
    {
        get => Dictionaries[(CurrentCHR, CurrentDictionary)].DictSortedByString;
        set => Dictionaries[(CurrentCHR, CurrentDictionary)].DictSortedByString = value;
    }
    public SortedDictionary<byte[], byte[]> DictSortByKey
    {
        get => Dictionaries[(CurrentCHR, CurrentDictionary)].DictSortByKey;
        set => Dictionaries[(CurrentCHR, CurrentDictionary)].DictSortByKey = value;
    }
    public Dictionary<byte[], byte[]> DictUnsortByKey
    {
        get => Dictionaries[(CurrentCHR, CurrentDictionary)].DictUnsortByKey;
        set => Dictionaries[(CurrentCHR, CurrentDictionary)].DictUnsortByKey = value;
    }

    public SortedDictionary<string, DictEntry> CHRSortedByString
    {
        get => CHRs[CurrentCHR].DictSortedByString;
    }

    private readonly SortedDictionary<(string, string), Dictionaries> Dictionaries;
    private readonly SortedDictionary<string, CHR> CHRs;

    public MyDictionary()
    {
        Dictionaries = new SortedDictionary<(string, string), Dictionaries>();
        CHRs = new SortedDictionary<string, CHR>();
    }

    public bool IsCHRValid(byte[] b) => CHRs[CurrentCHR].ByKey.ContainsKey(b);

    public static string GenerateFilename(bool ReadOriginal, string Filename)
    {
        string RetVal;
        if (File.Exists(string.Format(Constants.Path_To_x_both, Filename)))
        {
            RetVal = string.Format(Constants.Path_To_x_both, Filename);
        }
        else
        {
            RetVal = string.Format(ReadOriginal ? Constants.Path_To_x_orig : Constants.Path_To_x_new, Filename);

            if (!File.Exists(RetVal))
            {
                throw new FileNotFoundException($"File doesn't exist: {RetVal}");
            }
        }
        return RetVal;
    }

    public void LoadTableFiles(bool ReadOriginal, string CHRFile, string DictionaryFile, bool MirrorBlankDictionaryEntries)
    {
        if (string.IsNullOrEmpty(CHRFile))
        {
            throw new ArgumentNullException("CHR field in settings file cannot be empty.");
        }

        CurrentCHR = GenerateFilename(ReadOriginal, CHRFile);

        if (!string.IsNullOrEmpty(DictionaryFile))
        {
            CurrentDictionary = GenerateFilename(ReadOriginal, DictionaryFile);
        }

        if (!CHRs.ContainsKey(CurrentCHR))
        {
            CHRs.Add(CurrentCHR, new CHR());
            ReadCHRFile(CurrentCHR, out CHRs[CurrentCHR].ByKey, out CHRs[CurrentCHR].ByString, out int BiggestCHRKeyArraySize_);
            BiggestCHRKeyArraySize = BiggestCHRKeyArraySize_;
            CHRs[CurrentCHR].DictSortedByString = new SortedDictionary<string, DictEntry>();

            foreach (KeyValuePair<byte[], string> kvp in CHRs[CurrentCHR].ByKey)
            {
                if (kvp.Value.Length != 0)
                {
                    // Call this function solely to get the first character (characters in [brackets] count as a single character)
                    // It's kind of overkill to call this function when regex could've been used but whatever
                    _ = GetDictionaryAsString(kvp.Key, new DictionaryTables[] { DictionaryTables.CHR }, out string FirstChar);

                    if (!CHRs[CurrentCHR].DictSortedByString.ContainsKey(FirstChar))
                    {
                        CHRs[CurrentCHR].DictSortedByString.Add(FirstChar, new DictEntry());
                    }

                    if (!CHRs[CurrentCHR].DictSortedByString[FirstChar].ByKey.ContainsKey(kvp.Key))
                    {
                        CHRs[CurrentCHR].DictSortedByString[FirstChar].ByKey.Add(kvp.Key, new CharEntry(ByteVal: kvp.Key, StringVal: kvp.Value));

                        // If we're reading a trans table file, then we'll need to add the entry to "ByString" for inserting the new script.
                        if (!ReadOriginal)
                        {
                            if (CHRs[CurrentCHR].DictSortedByString[FirstChar].ByString.ContainsKey(kvp.Value))
                            {
                                throw new Exception($"CHR: ${CurrentCHR}; CHR key: {MyMath.FormatByteInBrackets(kvp.Key)}; CHR value: {kvp.Value}: Duplicate value found.");
                            }

                            CHRs[CurrentCHR].DictSortedByString[FirstChar].ByString.Add(kvp.Value, kvp.Key);
                        }
                        CHRs[CurrentCHR].DictSortedByString[FirstChar].MaxNumberOfCharacters = Math.Max(CHRs[CurrentCHR].DictSortedByString[FirstChar].MaxNumberOfCharacters, kvp.Value.Length);
                    }
                    else
                    {
                        throw new Exception($"CHR: {CurrentCHR}; CHR key: {MyMath.FormatByteInBrackets(kvp.Key)}; CHR value: '{kvp.Value}': Duplicate key found.");
                    }
                }
            }

        }

        Regex regex1 = new Regex(Constants.REGEX_SPLIT_STRING);

        //read dictionary

        if (!Dictionaries.ContainsKey((CurrentCHR, CurrentDictionary)))
        {
            Dictionaries.Add((CurrentCHR, CurrentDictionary), new Dictionaries());
            DictControlcode = new SortedDictionary<byte[], ControlCodeEntry>(new ByteComparer());
            DictSortedByString = new SortedDictionary<string, DictEntry>();


            ReadDictionaryFile
            (
                string.IsNullOrEmpty(CurrentDictionary) ? CurrentCHR : CurrentDictionary,
                MirrorBlankDictionaryEntries,
                out Dictionary<byte[], byte[]> DictUnsortByKey_,
                out SortedDictionary<byte[], byte[]> DictSortByKey_
            );

            DictUnsortByKey = DictUnsortByKey_;
            DictSortByKey = DictSortByKey_;
        }

        foreach (KeyValuePair<byte[], byte[]> kvp in Dictionaries[(CurrentCHR, CurrentDictionary)].DictSortByKey)
        {
            //ignore inserting blank entries.
            if (kvp.Value.Length != 0)
            {
                int LengthCompare = 0;
                string DictionaryString = GetDictionaryAsString(kvp.Value, new DictionaryTables[] { DictionaryTables.CHR }, out string FirstChar);

                //Dictionary entry is a control code
                if (DictControlcode.ContainsKey(kvp.Key))
                {
                    FirstChar = DictControlcode[kvp.Key].StringRepresentation;
                    LengthCompare = regex1.Split(DictControlcode[kvp.Key].StringRepresentation).Where(s => s != string.Empty).ToArray().Length;
                }
                //Dictionary entry is NOT a control code
                else
                {
                    LengthCompare = DictionaryString.Length;
                }

                if (!DictSortedByString.ContainsKey(FirstChar))
                {
                    DictSortedByString.Add(FirstChar, new DictEntry());
                }

                if (!DictSortedByString[FirstChar].ByKey.ContainsKey(kvp.Key))
                {
                    DictSortedByString[FirstChar].ByKey.Add(kvp.Key, new CharEntry(ByteVal: kvp.Value, StringVal: DictionaryString));
                    DictSortedByString[FirstChar].MaxNumberOfCharacters = Math.Max(DictSortedByString[FirstChar].MaxNumberOfCharacters, DictionaryString.Length);

                    if (DictSortedByString[FirstChar].ByString.ContainsKey(DictionaryString))
                    {
                        byte[] OriginalId = DictSortedByString[FirstChar].ByString[DictionaryString];
                        byte[] NewId = kvp.Key;

                        if (!OriginalId.SequenceEqual(NewId))
                        {
                            throw new Exception
                            (
                                $"Dictionary already contains an entry of the string value: {DictionaryString};\n" +
                                $"Original: {MyMath.FormatByteInBrackets(OriginalId)}; " +
                                $"Duplicate: {MyMath.FormatByteInBrackets(NewId)}\n" +
                                $"Check your dictionary for duplicate values and your SquishyText files."
                            );
                        }
                    }
                    else
                    {
                        DictSortedByString[FirstChar].ByString.Add(DictionaryString, kvp.Key);
                    }
                }
            }

            BiggestDictionaryKeyArraySize = Math.Max(BiggestDictionaryKeyArraySize, kvp.Key.Length);
        }

    }

    public string GetDictionaryAsString(byte[] b, Enums.DictionaryTables[] DictionaryInteractionType, out string FirstCharacter)
    {
        FirstCharacter = "";
        bool First = true;
        string Decoded = "";
        int i = 0;

        byte[] LookMeUp = null;
        int MaxByteLength = Math.Max(BiggestDictionaryKeyArraySize, BiggestCHRKeyArraySize);

        while (i < b.Length)
        {
            int LengthToTry = MaxByteLength;
            while (!(LengthToTry + i <= b.Length))
            {
                LengthToTry--;
            }

            DictionaryTables Found = DictionaryTables.Null;

            string FoundDictionaryValue = "";
            int ArgumentLength = 0;
            while (Found == DictionaryTables.Null && LengthToTry > 0)
            {
                LookMeUp = new byte[LengthToTry];
                Array.Copy(b, i, LookMeUp, 0, LengthToTry);

                if (DictionaryInteractionType.Contains(DictionaryTables.ControlCode) && DictControlcode.ContainsKey(LookMeUp))
                {
                    Found = DictionaryTables.ControlCode;
                    FoundDictionaryValue = DictControlcode[LookMeUp].StringRepresentation;
                    ArgumentLength = DictControlcode[LookMeUp].CodeArgumentLength;
                    break;
                }

                if (DictionaryInteractionType.Contains(DictionaryTables.Dictionary) && DictSortByKey.ContainsKey(LookMeUp) && DictSortByKey[LookMeUp].Length != 0)
                {
                    Found = DictionaryTables.Dictionary;
                    FoundDictionaryValue = GetDictionaryAsString(DictSortByKey[LookMeUp], DictionaryInteractionType, out _);
                    break;
                }

                if (DictionaryInteractionType.Contains(DictionaryTables.ChrDictionary) && DictSortByKey.ContainsKey(LookMeUp) && DictSortByKey[LookMeUp].Length != 0)
                {
                    // Chr dictionaries can't reference itself to prevent infinite loops.
                    Found = DictionaryTables.Dictionary;
                    FoundDictionaryValue = GetDictionaryAsString(LookMeUp, new Enums.DictionaryTables[] { Enums.DictionaryTables.CHR }, out _);
                    break;
                }

                if (DictionaryInteractionType.Contains(DictionaryTables.CHR) && IsCHRValid(LookMeUp))
                {
                    Found = DictionaryTables.CHR;
                    FoundDictionaryValue = GetCHRValue(LookMeUp);
                    break;
                }

                if (DictionaryInteractionType.Contains(DictionaryTables.Null))
                {
                    throw new Exception();
                }
                LengthToTry--;
            }

            if (Found != DictionaryTables.Null)
            {
                if (string.IsNullOrEmpty(FoundDictionaryValue))
                {
                    throw new Exception("Found control code/dictionary/CHR value cannot be empty.");
                }
                else
                {
                    Decoded += FoundDictionaryValue;
                }
            }
            else
            {
                Decoded += MyMath.FormatByteInBrackets(LookMeUp[0]);
            }

            if (LengthToTry == 0)
            {
                LengthToTry = 1;
            }

            i += LengthToTry;

            if (First)
            {
                First = false;
                FirstCharacter = FoundDictionaryValue;
            }

            while (ArgumentLength-- > Constants.CODE_ARGUMENT_LENGTH_NULL)
            {
                if (ROM.DATA.Length > i)
                {
                    Decoded += MyMath.FormatByteInBrackets(ROM.DATA[i++]);
                }
                else
                {
                    throw new Exception("String ends to early. Control code expected an argument.");
                }
            }
        }
        return Decoded;
    }

    public string GetCHRValue(byte[] b)
    {
        if (!CHRs[CurrentCHR].ByKey.ContainsKey(b))
        {
            return MyMath.FormatByteInBrackets(b);
        }
        return CHRs[CurrentCHR].ByKey[b];
    }

    public string GetCHRValue(byte[] b, out bool Found)
    {
        Found = true;
        if (!CHRs[CurrentCHR].ByKey.ContainsKey(b))
        {
            Found = false;
            return MyMath.FormatByteInBrackets(b);
        }
        return CHRs[CurrentCHR].ByKey[b];
    }

    public bool GetCHRByteValue(string Entry, out byte[] Key)
    {
        bool pass;
        Key = new byte[0];
        if (pass = CHRs[CurrentCHR].ByString.ContainsKey(Entry))
        {
            Key = CHRs[CurrentCHR].ByString[Entry];
        }
        return pass;
    }

    private void ReadCHRFile(string CHRPath, out SortedDictionary<byte[], string> CHRByKey, out SortedDictionary<string, byte[]> CHRByValue, out int BiggestKeyLength)
    {
        if (Global.Verbose)
        {
            Console.WriteLine($"Reading CHR file: {CHRPath}");
        }

        if (!File.Exists(CHRPath))
        {
            throw new FileNotFoundException($"The table file doesn't exist in the path: {CHRPath}.");
        }

        CHRByKey = new SortedDictionary<byte[], string>(new ByteComparer());
        CHRByValue = new SortedDictionary<string, byte[]>();
        BiggestKeyLength = 0;

        using (FileStream inputOpenedFile = File.Open(CHRPath, FileMode.Open))
        {
            using (StreamReader sr = new StreamReader(inputOpenedFile, Encoding.UTF8))
            {
                foreach (string fileLine in sr.ReadToEnd().Split(Constants.txt_newline.ToCharArray(), StringSplitOptions.RemoveEmptyEntries))
                {
                    if (Global.Debug)
                    {
                        Console.WriteLine(fileLine);
                    }

                    byte[] HexVal = GetHexValueFromFileLine(fileLine);
                    string StringVal = GetstringFromFileLine(fileLine);
                    if (StringVal != string.Empty)
                    {
                        if (!CHRByKey.ContainsKey(HexVal))
                        {
                            if (
                                StringVal.Contains(Constants.BYTE1.ToString()) ||
                                StringVal.Contains(Constants.BYTE2.ToString()) ||
                                StringVal.Contains(Constants.ACTION1.ToString()) ||
                                StringVal.Contains(Constants.ACTION2.ToString())
                                )
                            {
                                throw new Exception($"You cannot use special characters in the CHR file. You used the following special character for the value {MyMath.DecToHex(HexVal, Prefix.X)}: {StringVal}");
                            }

                            CHRByKey[HexVal] = StringVal;
                            CHRByValue[StringVal] = HexVal;
                            BiggestKeyLength = Math.Max(BiggestKeyLength, HexVal.Length);
                        }
                        else
                        {
                            throw new Exception($"Duplicate key {MyMath.FormatByteInBrackets(HexVal)}.");
                        }
                    }
                }
            }
        }
    }

    /// <summary>
    /// Changes encodingt to UTF7.
    /// 
    /// //https://stackoverflow.com/questions/14057434/how-can-i-transform-string-to-utf-8-in-c
    /// </summary>
    /// <param name="utf8String"></param>
    /// <returns></returns>
    private static string ProperEncoding(string utf8String)
    {
        byte[] utf8_Bytes = new byte[utf8String.Length];
        for (int i = 0; i < utf8String.Length; ++i)
        {
            utf8_Bytes[i] = (byte)utf8String[i];
        }

        return Encoding.UTF8.GetString(utf8_Bytes, 0, utf8_Bytes.Length);
    }

    private void ReadDictionaryFile
    (
        string filename,
        bool mirrorBlankDictionaryEntries,
        out Dictionary<byte[], byte[]> dictUnsortByKey,
        out SortedDictionary<byte[], byte[]> sortedTable
    )
    {
        if (!File.Exists(filename))
        {
            throw new FileNotFoundException($"The table file doesn't exist in the path: {filename}");
        }

        dictUnsortByKey = new Dictionary<byte[], byte[]>();
        sortedTable = new SortedDictionary<byte[], byte[]>(new ByteComparer());

        using (StreamReader sr = new StreamReader(filename, Encoding.UTF8, true))
        {
            string[] fileLines = sr.ReadToEnd().Split(Constants.txt_newline.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            foreach (string fileLine in fileLines)
            {
                //Ignore blank lines.
                if (fileLine.Contains(Constants.DICTIONARY_SPLIT))
                {
                    if (dictUnsortByKey.ContainsKey(GetHexValueFromFileLine(fileLine)))
                    {
                        throw new Exception("Duplicate key found: " + fileLine);
                    }

                    string TextToRight = GetstringFromFileLine(fileLine);

                    byte[] KeyValue = GetHexValueFromFileLine(fileLine);

                    if (TextToRight.Contains(Constants.ACTION1.ToString()) && TextToRight.Contains(Constants.ACTION2.ToString()))
                    {
                        int Action1LastIndex = TextToRight.LastIndexOf(Constants.ACTION1);

                        string Action = TextToRight.Substring(Action1LastIndex, TextToRight.LastIndexOf(Constants.ACTION2) - Action1LastIndex + 1);

                        int codeLength;

                        if (fileLine.Contains(Constants.CODE_LENGTH1.ToString()))
                        {
                            codeLength = int.Parse(fileLine.Split(Constants.CODE_LENGTH1, Constants.CODE_LENGTH2)[1]);
                        }
                        else
                        { 
                            codeLength = Constants.CODE_ARGUMENT_LENGTH_NULL; 
                        }

                        //extract byte values only.
                        TextToRight = Regex.Replace(TextToRight, @"(\(.*?\))|(\<.*?\>)|\{|\}", "");

                        DictControlcode.Add(KeyValue, new ControlCodeEntry(Action, codeLength));
                        dictUnsortByKey.Add(KeyValue, MyMath.HexToBytes(TextToRight));
                    }
                    else if (TextToRight.Contains(Constants.BYTE1.ToString()))
                    {
                        TextToRight = TextToRight.Replace(Constants.BYTE1.ToString(), "").Replace(Constants.BYTE2.ToString(), "");
                        dictUnsortByKey.Add(KeyValue, MyMath.HexToBytes(TextToRight));
                    }
                    // When the bytes aren't provided,
                    // check settings to see if we can use the key as the bytes.
                    else if (mirrorBlankDictionaryEntries)
                    {
                        if (KeyValue.Length == 0)
                        {
                            /*
                             * We trim the zeros off above. But we add
                             * a zero if it the value is zero. Otherwise,
                             * the array is empty.
                             */
                            KeyValue = new byte[] { 0x0 };
                        }

                        dictUnsortByKey.Add(KeyValue, KeyValue);
                    }
                    else
                    {
                        dictUnsortByKey.Add(KeyValue, new byte[0]);
                    }

                }
            }
        }
    }
    public string PrintEntireDictionary()
    {
        string s = "";
        s += "\n//Dictionary\n";
        foreach (KeyValuePair<byte[], byte[]> kvp in DictSortByKey)
        {
            s += $"{MyMath.DecToHex(kvp.Key, Prefix.X)}: {GetDictionaryAsString(DictSortByKey[kvp.Key], new DictionaryTables[] { DictionaryTables.CHR }, out _)}\n";

        }

        s += "\n//Control codes\n";
        foreach (KeyValuePair<byte[], ControlCodeEntry> kvp in DictControlcode)
        {
            s += $"{MyMath.DecToHex(kvp.Key, Prefix.X)}: {kvp.Value.StringRepresentation}\n";
        }

        s += "\n//CHR\n";
        foreach (KeyValuePair<byte[], string> kvp in CHRs[CurrentCHR].ByKey)
        {
            s += $"{MyMath.DecToHex(kvp.Key, Prefix.X)}: {kvp.Value}\n";
        }

        return s;
    }

    public void AddValueToDictionary(byte[] addEntryToDictionary)
    {
        if (string.IsNullOrEmpty(CurrentDictionary))
        {
            throw new Exception("A dictionary value cannot be added to a dictionary of an unspecified name.\n" +
                "Set IsDictionary to false or set DictionaryFile.");
        }
        
        int i = Dictionaries[(CurrentCHR, CurrentDictionary)].UnsortedIndex;

        if (DictUnsortByKey.Count <= i)
        {
            throw new Exception($"AddValueToDictionary: failed to add dictionary value of the UnsortedIndex of {MyMath.DecToHex(i,Prefix.X)} ({i}). " +
                "Make sure your dictionary file has enough entries to complement the currently read dictionary.");
        }

        KeyValuePair<byte[], byte[]> unsortedKvp = DictUnsortByKey.ElementAt(i);

        Dictionaries[(CurrentCHR, CurrentDictionary)].DictSortByKey[unsortedKvp.Key] = addEntryToDictionary;
        Dictionaries[(CurrentCHR, CurrentDictionary)].DictUnsortByKey[unsortedKvp.Key] = addEntryToDictionary;
        Dictionaries[(CurrentCHR, CurrentDictionary)].UnsortedIndex++;
    }

    /// <summary>
    /// Get the byte value (the first part, before the equal sign).
    /// </summary>
    /// <param name="str"></param>
    /// <returns></returns>
    private static byte[] GetHexValueFromFileLine(string str)
    {
        string ProperlyEncoded = ProperEncoding(str);
        int index = ProperlyEncoded.IndexOf(Constants.SEPERATOR);
        return MyMath.HexToBytes(str.Substring(0, index));
    }
    private static string GetstringFromFileLine(string str)
    {
        string ProperlyEncoded = ProperEncoding(str);
        int index = ProperlyEncoded.IndexOf(Constants.SEPERATOR);
        return str.Substring(index + Constants.SEPERATOR.Length).ToString();
    }
}