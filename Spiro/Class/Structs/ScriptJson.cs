﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

public class ScriptIndexTable
{
    [JsonProperty(Required = Required.Always)]
    public Dictionary<string, Entry> IndexTable { get; set; }

    public ScriptIndexTable()
    {
        IndexTable = new Dictionary<string, Entry>();
    }

    public class Entry
    {
        [JsonConverter(typeof(HexStringPairJsonConverter)), JsonProperty(Required = Required.Always)]
        public Dictionary<long, PointerTable> PointerTable { get; set; }

        public Entry()
        {
            PointerTable = new Dictionary<long, PointerTable>();
        }
    }

    public class PointerTable
    {
        [JsonProperty(Required = Required.Always)]
        public Dictionary<int, PointerTableEntry> PointerTableEntry { get; set; }

        public PointerTable()
        {
            PointerTableEntry = new Dictionary<int, PointerTableEntry>();
        }
    }

    public class PointerTableEntry
    {
        public string PointerLocation { get; set; }
        public string TextLocation { get; set; }
        public string ByteLength { get; set; }
        public SamePointerAs SamePointerAs { get; set; }
        [JsonProperty(Required = Required.Always)]
        public Dictionary<Enums.ScriptTypes, List<string>> Text { get; set; }
    }
}

public class SamePointerAs
{
    [JsonProperty(Required = Required.Always)]
    public string IndexTable { get; set; }
    [JsonConverter(typeof(HexStringToLongJsonConverter)), JsonProperty(Required = Required.Always)]
    public long PointerTableLocation { get; set; }
    [JsonProperty(Required = Required.Always)]
    public long EntryNo { get; set; }

    public SamePointerAs(string IndexTable, long PointerLocation, long EntryNo)
    {
        this.IndexTable = IndexTable;
        this.PointerTableLocation = PointerLocation;
        this.EntryNo = EntryNo;
    }

    public bool Equals(string IndexTable, long PointerTableLocation, long EntryNo)
    {
        return this.IndexTable == IndexTable && this.PointerTableLocation == PointerTableLocation && this.EntryNo == EntryNo;
    }
}

/// <summary>
/// Allows for the script data to have the addresses in hexadecimal
/// </summary>
public sealed class HexStringPairJsonConverter : JsonConverter
{
    public override bool CanConvert(Type objectType) => throw new NotImplementedException();
    public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
    {
        // This could have also sufficed but the indenting gets borked:
        // writer.WriteRawValue(JsonConvert.SerializeObject(value, Formatting.Indented));

        writer.WriteStartObject();
        foreach (KeyValuePair<long, ScriptIndexTable.PointerTable> kvp in (Dictionary<long, ScriptIndexTable.PointerTable>)value)
        {
            writer.WritePropertyName(MyMath.DecToHex(kvp.Key, Prefix.X));
            serializer.Serialize(writer, kvp.Value);
        }

        writer.WriteEndObject();   
    }
    public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
    {
        JObject jsonObject = JObject.Load(reader);
        Dictionary<long, ScriptIndexTable.PointerTable> NewScriptEntryList = new Dictionary<long, ScriptIndexTable.PointerTable>();

        foreach (KeyValuePair<string, ScriptIndexTable.PointerTable> kvp in jsonObject.ToObject<Dictionary<string, ScriptIndexTable.PointerTable>>())
        {
            NewScriptEntryList.Add(MyMath.HexToDec(kvp.Key), kvp.Value);
        }

        return NewScriptEntryList;
    }
}