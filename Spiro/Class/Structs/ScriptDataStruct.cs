﻿using System.Collections.Generic;

public class ScriptDataStruct
{
    public SortedDictionary<string, IndexTable> IndexTables { get; set; }

    public class IndexTable
    {
        // Key 1: Address
        // Key 2: Entry No
        public SortedDictionary<(long, long), Entry> Entries;

        public IndexTable()
        {
            Entries = new SortedDictionary<(long, long), Entry>();
        }
    }

    public class Entry
    {
        // Only used for the script output in case the user wants to know where the pointer is located
        public byte[] ScriptData { get; set; }
        public int MasterScriptLength { get; set; }
        public SamePointerAs SamePointerAs { get; set; }

        public Entry()
        {
            ScriptData = new byte[0];
        }
    }

    public ScriptDataStruct()
    {
        IndexTables = new SortedDictionary<string, IndexTable>();
    }
}