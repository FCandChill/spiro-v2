﻿public class Enums
{
    public enum PointerType
    {
        None = 0,
        LittleEndian = 1,
        ThreeByteMetalSladerDXPrimary = 2,
        ThreeByteMetalSladerDXPrimaryNoLength = 3,
        ThreeByteMetalSladerNESPrimary = 4,
        ThreeByteMetalSladerNESPrimaryNoLength = 5,
        ThreeByteMetalSladerNESTransPrimaryNoLength = 6,
        BigEndian = 7,
        Custom = 11,
    }

    public enum PointerGrouping
    {
        Normal,
        Single,
        SingleEntryFixedSize
    }

    public enum DialogueReadType
    {
        HasDelimiter = 0,
        HasLengthVar = 1,
        MarioPicrossSNES = 2,
    }

    public enum AddressConversion
    {
        None = 0,
        LoROM1 = 1,
        LoROM2 = 2,
        HiROM = 3,
        ExLoROM = 4,
        ExHiROM = 5,
    }

    public enum AutoLineBreak
    {
        None = 0,
        MetalSladerGlory = 1,
        TokimekiMiho = 2,
        TokimekiMihoMessage = 3,
    }

    public enum E_DataType
    {
        NoDataTpye,
        IntAddress,
        ByteArray,
    }

    public enum RenderType
    {
        Dictionary = 1,
        NoDictionaryMetalSladerNameCard = 2,
        KaettekitaMarioBrothersNagatanienWorldIntermission = 3,
        MarioPicrossSNES = 4,
    }

    public enum DictionaryTables
    {
        Null,
        CHR,
        Dictionary,
        ControlCode,
        // A dictionary that only references the table and not itself
        // If you use the normal dictionary enum and get a stack overflow exception, use this one instead
        ChrDictionary, 
    }

    public enum ScriptTypes
    {
        Menu,
        Original,
        New,
        Comment,

        Proof,
        Bytes,
    }
}