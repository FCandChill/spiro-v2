﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

public static class Setting
{
    public static Root settings;

    public static void ReadSettingsFile(string settingsFilePath)
    {
        settings = JsonConvert.DeserializeObject<Root>(new StreamReader(settingsFilePath).ReadToEnd());

        if (settings.SpiroVersion != Constants.VERSION)
        {
            throw new Exception();
        }

        ProcessFileMarkers(settings.Pointers.Read, out Dictionary<string, PointerReadWrite> read);
        ProcessFileMarkers(settings.Pointers.Write, out Dictionary<string, PointerReadWrite> write);

        settings.Pointers.Read = read;

        /*
         * Copy over the contents of read to write and then copy over the 
         * ones that were explicitly specifie in the settings file.
         * 
         * The "Read&Write" tag is used to specify read and write tags in one go.
         * You can also split them up if one entry is a bit different. For example,
         * if you want to expand how many dialogue entries there are for a certain pointer table,
         * or want to relocate the table.
         * 
         * However, maintaining duplicate entries for read and write is a chore, so copy over any read entries not in
         * write.
         */
        settings.Pointers.Write = new Dictionary<string, PointerReadWrite>(read);
        foreach(var kvp in write)
        {
            if (settings.Pointers.Write.ContainsKey(kvp.Key))
            {
                settings.Pointers.Write[kvp.Key] = kvp.Value;
            }
            else
            {
                settings.Pointers.Write.Add(kvp.Key, kvp.Value);
            }
        }

        // WriteableRange sanity check to prevent writing in the same space.
        foreach(var s1 in settings.Write.WriteableRange)
        {
            foreach (var s2 in settings.Write.WriteableRange)
            {
                // Make sure we're not checking a WriteableRange on itself
                if (s1.Key != s2.Key)
                {
                    // Make sure they're writing to the same file.
                    if (s1.Value.File == s2.Value.File)
                    {
                        long s1Start = s1.Value.StartAddress;
                        long s2Start = s2.Value.StartAddress;

                        long s1End = s1.Value.StartAddress + s1.Value.Size;
                        long s2End = s2.Value.StartAddress + s2.Value.Size;

                        if ((s2Start > s1Start && s2Start < s1End) || (s2End > s1Start && s2End < s1End))
                        {
                            throw new Exception($"{s1.Key} within the range of {s2.Key}");
                        }
                    }
                }
            }
        }
    }


    /// <summary>
    /// Take the value of "File" at the base of PointerReadWriteEntry and if it exists,
    /// apply it to every PointerFormat and RenderFormat entry.
    /// This is to save the enduser from having to enter the same file number multiple times.
    /// </summary>
    /// <param name="pw"></param>
    /// <param name="coll"></param>
    public static void ProcessFileMarkers(Dictionary<string, PointerReadWrite> pw, out Dictionary<string, PointerReadWrite> coll)
    {
        if (pw == null)
        {
            coll = null;
            return;
        }

        coll = new Dictionary<string, PointerReadWrite>();
        foreach (KeyValuePair<string, PointerReadWrite> kvp in pw)
        {
            // PointerFormat is an array for future expansion
            if (kvp.Value.File != "")
            {
                var copy = kvp.Value.Clone();
                foreach (var e in copy.PointerFormat)
                {
                    e.File = copy.File;
                }

                copy.RenderFormat.File = copy.File;
                copy.File = "";
                coll.Add(kvp.Key, copy);
            }
            else
            {
                coll.Add(kvp.Key, kvp.Value);
            }
        }
    }
}