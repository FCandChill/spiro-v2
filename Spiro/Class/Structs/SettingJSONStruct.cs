﻿//https://json2csharp.com/
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

public class DisplayReplaceCollection
{
    [JsonProperty(Required = Required.Always)]
    public string Find { get; set; }
    public string Replace { get; set; }
    public string ReplaceOriginal { get; set; }
    public string ReplaceNew { get; set; }
}

public class ScriptManager
{
    public string WriteScript { get; set; }
    public List<DisplayReplaceCollection> DisplayReplaceCollection { get; set; }
}

public class PointerReadWrite
{
    private string File_ = "";
    public string File { get => File_; set => File_ = value; }

    [JsonProperty(Required = Required.Always)]
    public PointerFormat[] PointerFormat { get; set; }
    [JsonProperty(Required = Required.Always)]
    public RenderFormat RenderFormat { get; set; }

    public PointerReadWrite Clone()
    {
        PointerReadWrite Copy = new PointerReadWrite
        {
            File = this.File,
            // Not at all necessary to properly shallow clone this, but better safe than sorry.
            RenderFormat = this.RenderFormat.Clone(),
            PointerFormat = (PointerFormat[])this.PointerFormat.Clone(),
        };

        return Copy;
    }
}
public class Entry : ICloneable
{
    // Only used for the script output in case the user wants to know where the pointer is located
    public long MasterPointerLocations { get; set; }
    public long MasterPointerAddresses { get; set; }
    public long MasterLength { get; set; }

    public object Clone()
    {
        return MemberwiseClone();
    }
}

public class PointerFormat
{
    private string File_ = "";
    public string File { get => File_; set => File_ = value; }
    [JsonConverter(typeof(SumArrayJsonConverter))]
    public long[] Address { set; get; }
    [JsonConverter(typeof(SumJsonConverter))]
    public long PcDifference { set; get; }
    [JsonProperty(Required = Required.Always)]
    public Enums.AddressConversion AddressConversion { set; get; }
    [JsonConverter(typeof(SumJsonConverter)), JsonProperty(Required = Required.Always)]
    public long EntryNumber { get; set; }
    [JsonProperty(Required = Required.Always)]
    public Enums.PointerType PointerType { get; set; }
    //[JsonProperty(Required = Required.Always)]
    public Enums.PointerGrouping PointerGrouping { get; set; }
    [JsonProperty(Required = Required.Always)]
    public int PointerLength { get; set; }
    public bool StoreOutOfOrderToSaveSpace { get; set; }
    public string[] CustomPointerFormat { get; set; }
    public long BytesBetween { get; set; }
    public bool IgnoreNegativeAndZeroPointers { set; get; }

    public PointerFormat Copy()
    {
        return new PointerFormat
        {
            File = this.File,
            Address = this.Address,
            PcDifference = this.PcDifference,
            AddressConversion = this.AddressConversion,
            EntryNumber = this.EntryNumber,
            PointerType = this.PointerType,
            PointerGrouping = this.PointerGrouping,
            PointerLength = this.PointerLength,
            StoreOutOfOrderToSaveSpace = this.StoreOutOfOrderToSaveSpace,
            CustomPointerFormat = this.CustomPointerFormat,
            BytesBetween = this.BytesBetween,
            IgnoreNegativeAndZeroPointers = this.IgnoreNegativeAndZeroPointers
        };
    }
}

public class RenderFormat
{
    private string File_ = "";
    public string File { get => File_; set => File_ = value; }
    public Enums.DialogueReadType DialogueReadType { get; set; }
    public string[] Delimiter { get; set; }
    [JsonProperty(Required = Required.Always)]
    public Enums.RenderType RenderType { get; set; }
    [JsonProperty(Required = Required.Always)]
    public Enums.DictionaryTables[] DictionaryInteractions { get; set; }
    [JsonProperty(Required = Required.Always)]
    public bool IsDictionary { get; set; }
    [JsonProperty(Required = Required.Always)]
    public string CHRFile { get; set; }
    public string DictionaryFile { get; set; }
    public int PixelsPerLine { get; set; }
    public Enums.AutoLineBreak AutoLineBreak { get; set; }
    public bool MirrorBlankDictionaryEntries { get; set; }
    public bool MergeDictionaryAndCHRFile { get; set; }
    public string SquishyTextFile { get; set; }
    public bool SquishyTextFileUseregex { get; set; }
    public string[] SpecialPointerFormat { get; set; }
    public bool ReverseTableEndianness { get; set; }
    [JsonConverter(typeof(SumJsonConverter))]
    public long FixedSize { set; get; }
    private long DelimiterCount_ = 1;
    [JsonConverter(typeof(SumJsonConverter))]
    public long DelimiterCount { get => DelimiterCount_; set => DelimiterCount_ = value; }

    internal RenderFormat Clone()
    {
        var r = new RenderFormat
        {
            File = this.File,
            DialogueReadType = this.DialogueReadType,
            RenderType = this.RenderType,
            IsDictionary = this.IsDictionary,
            CHRFile = this.CHRFile,
            DictionaryFile = this.DictionaryFile,
            PixelsPerLine = this.PixelsPerLine,
            AutoLineBreak = this.AutoLineBreak,
            MirrorBlankDictionaryEntries = this.MirrorBlankDictionaryEntries,
            SquishyTextFile = this.SquishyTextFile,
            SquishyTextFileUseregex = this.SquishyTextFileUseregex,
            ReverseTableEndianness = this.ReverseTableEndianness,
            FixedSize = this.FixedSize,
            DelimiterCount = this.DelimiterCount
        };

        if (this.SpecialPointerFormat != null)
        {
            r.SpecialPointerFormat = (string[])this.SpecialPointerFormat.Clone();
        }

        if (this.DictionaryInteractions != null)
        {
            r.DictionaryInteractions = (Enums.DictionaryTables[])this.DictionaryInteractions.Clone();
        }

        if (this.Delimiter != null)
        {
            r.Delimiter = (string[])this.Delimiter.Clone();
        }

        return r; 
    }
}

public class Pointers
{
    [JsonProperty("Read&Write")]
#pragma warning disable IDE0051 // Remove unused private members
    private Dictionary<string, PointerReadWrite> ReadWrite { set { Read = value; Write = value; } }
#pragma warning restore IDE0051 // Remove unused private members

    [JsonProperty("Read")]
    public Dictionary<string, PointerReadWrite> Read { get; set; }

    [JsonProperty("Write")]
    public Dictionary<string, PointerReadWrite> Write { get; set; }
}

public class WriteableRange
{
    [JsonProperty(Required = Required.Always)]
    public string File { get; set; }
    [JsonConverter(typeof(SumJsonConverter)), JsonProperty(Required = Required.Always)]
    public long StartAddress { get; set; }
    [JsonConverter(typeof(SumJsonConverter)), JsonProperty(Required = Required.Always)]
    public long Size { get; set; }
}

public class WriteRegion
{
    [JsonProperty(Required = Required.Always)]
    public string[] EntryOwners { get; set; }
    [JsonProperty(Required = Required.Always)]
    public string[] WriteableAddressRanges { get; set; }
}

public class Write
{
    [JsonProperty(Required = Required.Always)]
    public Dictionary<string, WriteableRange> WriteableRange { get; set; }
    [JsonProperty(Required = Required.Always)]
    public WriteRegion[] WriteRegion { get; set; }
}

public class Misc
{
    // This feature is useful for identifying where text is stored
    // Additinally, it's useful to spot if you still have some more text to dump
    public bool BlankOutTextDataAfterRead { get; set; }
    [JsonConverter(typeof(HexStringToByteJsonConverter))]
    public byte BlankOutByte { get; set; }

    /// <summary>
    /// Option to enable or disable the SamePointerAs feature.
    /// If enabled, the script dump will only include the first instance of a line
    /// if two entries reference the same text location.
    /// </summary>
    public bool MultiplePointersReferencingAString { get; set; }

    /// <summary>
    /// Determines where to obtain the file to write the new pointers to.
    /// 
    /// When writing pointers, the utility by default writes to the file specified under 
    /// "Write.WriteableRange.$VALUE.File" instead of "Pointers.Write.$VALUE.File".
    /// I'll admit this is logically backwards, but it allows more opportunities to utilize
    /// "Read&Write" instead of "Read" and "Write" which takes up way more space.
    /// 
    /// Setting "GetFileToWritePointerFromPointersField" to true writes pointers specified
    /// in the latter field instead of the former field. This is useful when working with multiple files
    /// where pointers reference different files.
    /// </summary>
    public bool GetFileToWritePointerFromPointersField { get; set; }
}

public class Script
{
    [JsonProperty(Required = Required.Always)]
    public string ScriptPath { get; set; }

    [JsonProperty(Required = Required.Always)]
    public bool Original { get; set; }

    [JsonProperty(Required = Required.Always)]
    public bool Comment { get; set; }

    [JsonProperty(Required = Required.Always)]
    public bool New { get; set; }

    [JsonProperty(Required = Required.Always)]
    public bool Bytes { get; set; }

    [JsonProperty(Required = Required.Always)]
    public bool Menu { get; set; }

    [JsonProperty(Required = Required.Always)]
    public bool Proof { get; set; }
}

public class Root
{
    [JsonProperty(Required = Required.Always)]
    public int SpiroVersion { get; set; }
    [JsonProperty(Required = Required.Always)]
    public Dictionary<string, string> Files { get; set; }
    [JsonProperty(Required = Required.Always)]
    public Misc Misc { get; set; }
    [JsonProperty(Required = Required.Always)]
    public Script Scripts { get; set; }
    public ScriptManager ScriptManager { get; set; }
    [JsonProperty(Required = Required.Always)]
    public Pointers Pointers { get; set; }
    [JsonProperty(Required = Required.Always)]
    public Write Write { get; set; }
}

public sealed class SumJsonConverter : JsonConverter
{
    public override bool CanConvert(Type objectType) => throw new NotImplementedException();
    public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer) => throw new NotImplementedException();
    public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
    {
        if (Global.Debug)
        {
            Console.WriteLine($"ReadJson: Reading property ${reader.Path}");
        }

        if (reader.Value != null)
        {
            return ProcessHexSumString(reader.Value.ToString());
        }
        return -1;
    }

    public static long ProcessHexSumString(string rawString)
    {
        if (Global.Debug)
        {
            Console.WriteLine($"ProcessHexSumString: rawString: {rawString}");
        }

        if (rawString.Contains("+") || rawString.Contains("-"))
        {
            string[] a_chunks = (new Regex(@"(\+)|(\-)")).Split(rawString).Where(s => s != string.Empty).ToArray();

            long Address = 0;
            bool IsAdd = true;
            foreach (string s in a_chunks)
            {

                switch (s)
                {
                    case "+":
                        IsAdd = true;
                        break;
                    case "-":
                        IsAdd = false;
                        break;
                    default:
                        long Multipler = IsAdd ? 1 : -1;
                        Address += Multipler * (long)MyMath.HexToUInt64(s);
                        break;
                }
            }
            return Address;
        }
        else
        {
            if (MyMath.IsHex(rawString))
            {
                return (long)MyMath.HexToUInt64(rawString);
            }
            else
            {
                if (long.TryParse(rawString, out long Result))
                {
                    return Result;
                }
                else
                {
                    throw new ArgumentException($"Couldn't parse the following string as a long: \"{rawString}\"");
                }
            }
        }
    }
}

public sealed class SumArrayJsonConverter : JsonConverter
{
    public override bool CanConvert(Type objectType) => throw new NotImplementedException();
    public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer) => throw new NotImplementedException();
    public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
    {
        string[] value = JArray.Load(reader).ToObject<string[]>();

        
        long[] RetVal = new long[0];

        if (value != null)
        {
            RetVal = new long[value.Length];

            for (int i = 0; i < value.Length; i++)
            {
                RetVal[i] = SumJsonConverter.ProcessHexSumString(value[i]);
            }
        }
        return RetVal;
    }
}

public sealed class DelimiterConverter : JsonConverter
{
    public override bool CanConvert(Type objectType) => throw new NotImplementedException();
    public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer) => throw new NotImplementedException();
    public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
    {
        string[][] value = JArray.Load(reader).ToObject<string[][]>();

        byte[][] b;
        if (value != null)
        {
            b = new byte[value.Length][];

            for (int i = 0; i < value.Length; i++)
            {
                b[i] = new byte[value[i].Length];
                for (int j = 0; j < b[i].Length; j++)
                {
                    b[i][j] = (byte)MyMath.HexToDec(value[i][j]);
                }
            }
        }
        else
        {
            b = null;
        }

        return b;
    }
}

public sealed class HexStringToByteJsonConverter : JsonConverter
{
    public override bool CanConvert(Type objectType) => typeof(byte).Equals(objectType);
    public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer) => writer.WriteValue(MyMath.DecToHex((long)value, Prefix.X));
    public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer) => (byte)MyMath.HexToDec((string)reader.Value);
}

public sealed class HexStringToLongJsonConverter : JsonConverter
{
    public override bool CanConvert(Type objectType) => typeof(long).Equals(objectType);
    public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer) => writer.WriteValue(MyMath.DecToHex((long)value, Prefix.X));
    public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer) => (long)MyMath.HexToDec((string)reader.Value);
}