﻿using System;
using System.Collections.Generic;
using System.IO;

public static class ROM
{
    private static string CURRENT_ROM_;
    public static string CURRENT_ROM
    {
        get => CURRENT_ROM_;

        set
        {
            if (!ALL.ContainsKey(value))
            {
                throw new Exception($"File key does not exist: {value}. Make sure it exists in the settings file and that the path is correct.");
            }

            CURRENT_ROM_ = value;
        }
    }
    public static byte[] DATA
    { 
        get => ALL[CURRENT_ROM];
        set => ALL[CURRENT_ROM] = value;
    }
    public static Dictionary<string, byte[]> ALL;

    public static void ReadRoms()
    {
        ALL = new Dictionary<string, byte[]>();

        foreach(var kvp in Setting.settings.Files)
        {
            if (File.Exists(Setting.settings.Files[kvp.Key]))
            {
                if (!ALL.ContainsKey(kvp.Key))
                {
                    ALL.Add(kvp.Key, File.ReadAllBytes(Setting.settings.Files[kvp.Key]));
                }
                else
                {
                    throw new Exception($"ROM ALL array already contains the key: {kvp.Key}");
                }
            }
            else
            {
                if (Global.Verbose)
                {
                    Console.WriteLine($"File not found: {Setting.settings.Files[kvp.Key]}");
                }
            }
        }
    }
}