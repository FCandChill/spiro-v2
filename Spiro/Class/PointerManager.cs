using System;
using System.Linq;

public class PointerManager
{
    public long CreateAddressMetalSladerSNES(long PC_Address, int Length)
    {
        long SnesAddress = PC_Address;

        byte byte1, byte2, byte3;
        byte1 = byte3 = 0x0;

        //add length bits.
        byte1 |= (byte)((Length & 0x18) << 3);
        byte3 |= (byte)((Length & 0x7) << 5);

        //now the address bits.
        byte[] bytes = BitConverter.GetBytes(SnesAddress);
        byte2 = bytes[0];
        byte1 |= (byte)(((bytes[2] - 0x20) << 2) & 0x3C);

        byte1 |= (byte)((bytes[1] & 0x60) >> 5);
        byte3 |= (byte)(bytes[1] & 0x1F);

        byte[] bytes__ = new byte[] { byte1, byte2, byte3 };
        GetAddressMetalSladerSNES(bytes__, out long PC_Address_, out long Length_);

        if (Global.Verbose)
        {
            Console.WriteLine($"Address: 0x{PC_Address:X5}. Length #${Length:X2}");
        }

        //Sanity check.
        if (PC_Address_ != PC_Address || Length_ != Length)
        {
            if (Global.Debug)
            {
                Console.WriteLine($"Address: 0x{PC_Address_:X5}. Length #${Length_:X2}. Bad conversion.");
            }

            throw new Exception("Conversion unsuccessful. Aborting.");
        }

        return BitConverter.ToUInt32(new byte[] { byte3, byte2, byte1, 0 }, 0);
    }

    /*
 * Address layout (in bits)
 * ------------------------
 * 
 * a, b     = length bytes
 * x, y, z  = Address bytes
 *       
 * a1 a2 x1 x2 x3 x4 x5 x6      y1 y2 y3 y4 y5 y6 y7 y8     b1 b2 b3 z1 z2 z3 z4 z5
 * 
 * length   = - - - a1 a2 b1 b2 b3
 * address  =         
 *      
 *      Original:
 *                          x1 x2 x3 
 *      cccccccc =  0  0  0  0  0 x1 x2 x3 
 *      dddddddd = x4 x5 x6 z1 z2 z3 z4 z5
 *      eeeeeeee = y1 y2 y3 y4 y5 y6 y7 y8
 *      
 *      Final address = cccccccc dddddddd eeeeeeee
 */

    public long CreateAddressMetalSladerNES(long pcAddress, int length, bool isTranslation = false)
    {
        if (length > 0b11111)
        {
            throw new Exception($"CreateAddressMetalSladerNES: Length must be sorter than 31 characters. Line length: {length}");
        }

        long SnesAddress = pcAddress;
        byte byte1, byte2, byte3;
        byte1 = byte3 = 0x0;

        byte1 |= (byte)((length & 0x18) << 3);
        byte3 |= (byte)((length & 0x7) << 5); //add length bits.

        //now the address bits.
        byte[] bytes = BitConverter.GetBytes(SnesAddress);
        byte2 = bytes[0];

        byte1 |= (byte)((bytes[2] << 3) & 0x78);
        byte1 |= (byte)((bytes[1] & 0xE0) >> 5);
        byte3 |= (byte)((bytes[1] & 0x1F));

        GetAddressMetalSladerNES(new byte[] { byte1, byte2, byte3 }, out long pcAddress_, out long length_, isTranslation);

        if (Global.Debug)
        {
            Console.WriteLine($"Address: 0x{pcAddress:X5}. Length #${length:X2}");
        }

        //Sanity check.
        if (pcAddress_ != pcAddress || (length_ != length && (length != 0)))
        {
            if (Global.Debug)
            {
                Console.WriteLine($"Address: 0x{pcAddress_:X5}. Length #${length_:X2}. Bad conversion.");
            }

            throw new Exception("Conversion unsuccessful. Aborting.");
        }

        return BitConverter.ToUInt32(new byte[] { byte3, byte2, byte1, 0 }, 0);
    }

    public void GetAddressMetalSladerSNES(byte[] b, out long PointerAddress, out long Length)
    {
        /*
         * Address layout (in bits)
         * ------------------------
         * 
         * a, b     = length bytes
         * x, y, z  = Address bytes
         *       
         * a1 a2 x1 x2 x3 x4 x5 x6      y1 y2 y3 y4 y5 y6 y7 y8     b1 b2 b3 z1 z2 z3 z4 z5
         * 
         * length   = - - - a1 a2 b1 b2 b3
         * address  =         
         *                                   x            
         *      cccccccc =  0  0  1  0 x1 x2 x3 x4
         *      dddddddd =  1 x5 x6 z1 z2 z3 z4 z5
         *      eeeeeeee = y1 y2 y3 y4 y5 y6 y7 y8
         *      
         *      Final address = cccccccc dddddddd eeeeeeee
         *                      
         *   Smallest address = 00100000 10000000 00000000 => 0x104000
         *   Biggest address  = 00101111 11111111 11111111 => 0x2FFFFF
         *   							 ^
	     *   The bit with the arrows under it must always be 1. So not all addresses
         *   can be pointed to between 0x208000 and 0x2FFFFF. 
         *   The pointers cover a wide range, so it makes you wonder why the programmer
         *   even bothered with dictionary compression. It may be an artifact
         *   from the NES port.
         */

        if (b.Length != 3)
        {
            throw new Exception("Pointer must be a length of 3.");
        }

        // 24-bit UNPACKER
        int byte1 = b[0] & 0x3f;
        int byte2 = b[2] & 0x1f;
        int byte3 = b[1];

        byte2 |= ((byte1 & 0x03) << 5) + 0x80;
        byte1 = 0x20 + ((byte1 & 0xfc) >> 2);

        long ptr = ((byte1 << 16) + (byte2 << 8) + (byte3 << 0));

        //Pointer that points to dialogue.
        PointerAddress = ptr;

        Length = (b[0] & 0xc0) >> 3;
#pragma warning disable CS0675 // Bitwise-or operator used on a sign-extended operand
        Length |= (b[2] & 0xe0) >> 5;
#pragma warning restore CS0675 // Bitwise-or operator used on a sign-extended operand

        if (Global.Debug)
        {
            Console.WriteLine($"Address: 0x{PointerAddress:X5}. Length #${Length:X2}");
        }
    }

    public void GetAddressMetalSladerNES(byte[] b, out long PointerAddress, out long Length, bool isRomExpanded = false)
    {
        /*
         * Address layout (in bits)
         * ------------------------
         * 
         * a, b     = length bytes
         * x, y, z  = Address bytes
         *       
         * a1 * x1 x2 x3 x4 x5 x6      y1 y2 y3 y4 y5 y6 y7 y8     b1 b2 b3 z1 z2 z3 z4 z5
         * 
         *   * = Used a length bit for dictionary. Dialogue entries never use the length bit,
         *   therefore, the translation uses this to use the expanded PRG.
         *   So there asterisk can represent a2 and x0.
         * 
         * 
         * 
         * length   = - - - a1 a2 b1 b2 b3
         * address  =         
         *      
         *      Original:
         * 
         *      cccccccc =  0  0  0  0  0 x1 x2 x3 
         *      dddddddd = x4 x5 x6 z1 z2 z3 z4 z5
         *      eeeeeeee = y1 y2 y3 y4 y5 y6 y7 y8
         *      
         *      Translation:
         * 
         *      cccccccc =  0  0  0 a1 a2 x1 x2 x3 
         *      dddddddd = x4 x5 x6 z1 z2 z3 z4 z5
         *      eeeeeeee = y1 y2 y3 y4 y5 y6 y7 y8
         *      
         *      
         *      Final address = cccccccc dddddddd eeeeeeee
         */

        if (b.Length != 3)
        {
            throw new Exception("Pointer must be a length of 3.");
        }

        // 24-bit UNPACKER

        int byte1 = b[0] & 0x3F;

        if (isRomExpanded)
        {
            byte1 = b[0] & 0x7F;
        }

        int byte2 = b[2] & 0x1f;
        int byte3 = b[1];

        byte2 |= ((byte1 & 0x07) << 5);
        //byte2 |= ((byte1 & 0x04) << 6);

        if (!isRomExpanded)
        {
            byte1 = ((byte1 & 0x3c) >> 3);
        }
        else
        {
            byte1 = ((byte1 & 0x7C) >> 3);
        }

        long ptr = ((byte1 << 16) + (byte2 << 8) + (byte3 << 0));

        //Pointer that points to dialogue.
        PointerAddress = ptr;

        Length = (b[0] & 0xc0) >> 3;
#pragma warning disable CS0675 // Bitwise-or operator used on a sign-extended operand
        Length |= (b[2] & 0xe0) >> 5;
#pragma warning restore CS0675 // Bitwise-or operator used on a sign-extended operand
    }

    public void GetAddressCustomFormat(byte[] b, string[] Format, out long PointerAddress)
    {
        byte[] PointerArray = new byte[8];

        if (Format == null || Format.Length == 0)
        {
            throw new Exception("Custom format array cannot be empty.");
        }

        for (int i = 0; i < Format.Length; i++)
        {
            string Val = Format[i];

            if (Val.Contains('('))
            {
                Val = Val.Replace("(", "").Replace(")", "");
                int j = int.Parse(Val[1].ToString());
                PointerArray[j] = b[i];
            }
            else
            {
                if (Val != "XX" && b[i] != MyMath.HexToDec(Val))
                {
                    throw new Exception($"Custom format mismatch. Excepted: 0x{Val:X2}; Got: 0x{b[i]:X2}; Index: {i}");
                }
            }
        }

        PointerAddress = (long)BitConverter.ToUInt64(PointerArray, 0);
    }

    public byte[] CreateAddressCustomFormat(long pcAddress, string[] customPointerFormat)
    {
        byte[] pointerAsBytes = BitConverter.GetBytes(pcAddress);

        if (customPointerFormat == null)
        {
            throw new Exception("CreateAddressCustomFormat: CustomPointerFormat cannot be null");
        }

        byte[] newPointer = new byte[customPointerFormat.Length];

        for (int i = 0; i < customPointerFormat.Length; i++)
        {
            string Val = customPointerFormat[i];

            if (Val.Contains('('))
            {
                Val = Val.Replace("(", "").Replace(")", "");

                //get X value in BX
                int j = int.Parse(Val[1].ToString());

                //because we reversed the order, we d
                newPointer[i] = pointerAsBytes[j];
            }
            else
            {
                newPointer[i] = (byte)MyMath.HexToDec(Val);
            }
        }
        return newPointer;
    }

    public void ReadLittleEndian(byte[] b, out long pointerAddress)
    {
        if (b.Length == 0 || b == null)
        {
            throw new Exception("ReadLittleEndian: byte array is null or empty");
        }

        byte[] lb = new byte[8];
        Array.Copy(b, 0, lb, 0, b.Length);
        pointerAddress = (long)BitConverter.ToUInt64(lb, 0);
    }
    public void ReadBigEndian(byte[] b, out long pointerAddress)
    {
        if (b.Length == 0 || b == null)
        {
            throw new Exception("ReadBigEndian: byte array is null or empty");
        }

        byte[] lb = new byte[8];
        Array.Copy(b.Reverse().ToArray(), 0, lb, 0, b.Length);
        pointerAddress = (long)BitConverter.ToUInt64(lb, 0);
    }

    public byte[] CreatePointerLittleEndian(long Address, long Length)
    {
        byte[] Converted = BitConverter.GetBytes(Address);
        byte[] Done = new byte[Length];

        Array.Copy(Converted, 0, Done, 0, Length);

        return Done;
    }
}