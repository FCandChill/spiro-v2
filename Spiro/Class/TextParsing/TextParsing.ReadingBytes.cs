﻿using System;
using System.Collections.Generic;
using System.Linq;
using static Enums;

public partial class TextParsing
{
    public List<byte> ReadByteStreamWithPointerInSize(Entry e, byte[] buffer)
    {
        byte[] byteData = new byte[e.MasterLength];

        long pcAddress = e.MasterPointerAddresses;
        long length = e.MasterLength;

        Array.Copy(ROM.DATA, (int)pcAddress, byteData, 0, length);

        BlankOutData(pcAddress, length, buffer);

        return byteData.ToList();
    }

    public string ReadByteStreamWithPointer(long startAddress, string key, PointerReadWrite prw, out List<byte> scriptBytes, out List<byte> scriptBytesWithoutDelimiter, byte[] buffer, bool usesDelimiter = false, int byteSize = Constants.GENERIC_NULL)
    {
        DictionaryTables[] dictionaryInteractionType = prw.RenderFormat.DictionaryInteractions;
        bool reverseTableEndianness = prw.RenderFormat.ReverseTableEndianness;

        scriptBytes = new List<byte>();
        scriptBytesWithoutDelimiter = new List<byte>();

        if (prw.RenderFormat.DelimiterCount < 1)
        {
            throw new Exception("ReadByteStreamWithPointerAndDelim requires a DelimiterCount greater than 0.");
        }

        int delimiterCount = 0;

        if (usesDelimiter && (prw.RenderFormat.Delimiter == null || prw.RenderFormat.Delimiter.Length == 0))
        {
            throw new Exception("ReadByteStreamWithPointerAndDelim requires a delimiter in the settings.");
        }

        if (!usesDelimiter && byteSize == Constants.GENERIC_NULL)
        {
            throw new Exception("ReadByteStreamWithPointerAndDelim requires either delimiter or byteSize setting.");
        }

        if (startAddress < 0)
        {
            if (!Setting.settings.Pointers.Read[key].PointerFormat[Constants.DefaultPointerIndex].IgnoreNegativeAndZeroPointers)
            {
                throw new Exception("Pointer value not positive with no override.");
            }
        }

        string scriptAsText = "";
        byte[] lookMeUp = null;
        long i = startAddress;

        int MaxByteLength = Math.Max(Chara.BiggestDictionaryKeyArraySize, Chara.BiggestCHRKeyArraySize);
        while (true)
        {
            if (i > buffer.Length)
            {
                throw new Exception("Attempted to readoutside the ROM.");
            }

            if (scriptBytes.Count >= 10000000)
            {
                throw new Exception($"The byte count is {scriptBytes.Count}. It's getting rather long, so you're probably reading invalid data at this point.");
            }

            int LengthToTry = MaxByteLength;
            while (!(i + LengthToTry <= buffer.Length))
            {
                LengthToTry--;
            }

            DictionaryTables Found = DictionaryTables.Null;

            while (Found == DictionaryTables.Null && LengthToTry > 0)
            {
                lookMeUp = new byte[LengthToTry];
                Array.Copy(buffer, i, lookMeUp, 0, LengthToTry);

                if (reverseTableEndianness)
                {
                    lookMeUp = lookMeUp.Reverse().ToArray();
                }

                switch (dictionaryInteractionType)
                {
                    case DictionaryTables[] a when a.Contains(DictionaryTables.Dictionary):
                    case DictionaryTables[] b when b.Contains(DictionaryTables.ControlCode):
                    case DictionaryTables[] c when c.Contains(DictionaryTables.ChrDictionary):
                        if (Chara.DictControlcode.ContainsKey(lookMeUp))
                        {
                            Found = DictionaryTables.ControlCode;
                            goto Out;
                        }

                        if (Chara.DictSortByKey.ContainsKey(lookMeUp) && Chara.DictSortByKey[lookMeUp].Length != 0)
                        {
                            Found = DictionaryTables.Dictionary;
                            goto Out;
                        }
                        break;
                    default:
                        break;
                }

                if (dictionaryInteractionType.Contains(DictionaryTables.CHR) && Chara.IsCHRValid(lookMeUp))
                {
                    Found = DictionaryTables.CHR;
                    break;
                }

                LengthToTry--;
            }

            Out:

            if (LengthToTry == 0)
            {
                LengthToTry = 1;
            }

            i += LengthToTry;

            if (Found != DictionaryTables.Null)
            {
                string foundDictionaryValue = "";
                int argumentLength = 0;

                switch (Found)
                {
                    case DictionaryTables.ControlCode:
                        foundDictionaryValue = Chara.DictControlcode[lookMeUp].StringRepresentation;
                        argumentLength = Chara.DictControlcode[lookMeUp].CodeArgumentLength;
                        break;
                    case DictionaryTables.Dictionary:
                        foundDictionaryValue = Chara.GetDictionaryAsString(Chara.DictSortByKey[lookMeUp], dictionaryInteractionType, out _);
                        break;
                    case DictionaryTables.CHR:
                        foundDictionaryValue = Chara.GetCHRValue(lookMeUp);
                        break;
                    case DictionaryTables.Null:
                        throw new Exception();
                }

                if (string.IsNullOrEmpty(foundDictionaryValue))
                {
                    throw new Exception("Found control code/dictionary/CHR value cannot be empty.");
                }

                scriptAsText += foundDictionaryValue;

                // If we reversed the key, reverse it back.
                if (reverseTableEndianness)
                {
                    lookMeUp = lookMeUp.Reverse().ToArray();
                }

                bool isOver = false;

                if (usesDelimiter && prw.RenderFormat.Delimiter.Contains(foundDictionaryValue))
                {
                    delimiterCount++;
                    if (prw.RenderFormat.DelimiterCount == delimiterCount)
                    {
                        isOver = true;
                        scriptBytesWithoutDelimiter = new List<byte>(scriptBytes);
                    }
                }

                scriptBytes = scriptBytes.Concat(lookMeUp).ToList();

                if (byteSize > 0)
                {
                    if (startAddress + byteSize == scriptBytes.Count)
                    {
                        isOver = true;
                    }

                    if (prw.PointerFormat[0].Address[0] == 0x02AEC)
                    {

                    }

                    if (startAddress + byteSize < scriptBytes.Count)
                    {
                        throw new Exception($"ReadByteStreamWithPointer: size exceeded limit of { MyMath.DecToHex(byteSize, Prefix.X) }. Current size: { MyMath.DecToHex(scriptBytes.Count, Prefix.X) }");
                    }
                }

                //For Earthbound's "(Multiple-Address Reference Pointer MULTIPLIER)"
                if (foundDictionaryValue.Contains("MULTIPLIER"))
                {
                    argumentLength *= buffer[i];
                }

                while (argumentLength-- > Constants.CODE_ARGUMENT_LENGTH_NULL)
                {
                    if (buffer.Length > i)
                    {
                        scriptBytes.Add(buffer[i]);
                        scriptAsText += MyMath.FormatByteInBrackets(buffer[i++]);
                    }
                    else
                    {
                        throw new Exception($"String ends to early. Control code \"{ foundDictionaryValue }\" expected an argument.");
                    }
                }

                if (isOver)
                {
                    break;
                }
            }
            else
            {
                scriptAsText += MyMath.FormatByteInBrackets(lookMeUp[0]);
                scriptBytes.Add(lookMeUp[0]);
            }
        }


        // Don't blank out the delimiter character becuase data can overlap
        // and make the editor read longer than it should.
        BlankOutData(startAddress, scriptBytesWithoutDelimiter.Count, buffer);

        return scriptAsText;
    }

    public string ReadByteMarioPicrossSNES(long pos, string key, PointerReadWrite prw, out List<byte> scriptBytes, out List<byte> scriptBytesWithoutDelimiter)
    {
        // Start of logic at c01809 in SNES memory
        int StackValue = 0x4;
        List<byte> byteList = new List<byte>();
        string scriptText = "";

        while (true)
        {
            byte b = ROM.DATA[pos];
            byteList.Add(b);
            pos++;
            /*
             * c0181a cmp $01,s     [001fb9]
             * c0181c beq $1832     [c01832]
             */
            if (b == StackValue)
            {
                int puzzleNameLength = ((ROM.DATA[pos + 1] << 8) + ROM.DATA[pos]) - 4;

                byteList.Add(ROM.DATA[pos++]);
                byteList.Add(ROM.DATA[pos++]);
                scriptText += MyMath.FormatBytesInBrackets(byteList.ToArray());

                byte[] picrossPuzzleName = new byte[puzzleNameLength];
                Array.Copy(ROM.DATA, pos, picrossPuzzleName, 0, puzzleNameLength);

                // Get the puzzle name
                scriptText += ReadByteStreamWithPointer(0, key, prw, out List<byte> scriptAsBytes_, out List<byte> _, picrossPuzzleName, false, picrossPuzzleName.Length);
                pos += picrossPuzzleName.Length;
                byteList = byteList.Concat(scriptAsBytes_).ToList();

                // Read the rest of the puzzle data
                _ = ReadByteStreamWithPointer(pos, key, prw, out scriptAsBytes_, out List<byte> scriptBytesWithoutDelimiter_, ROM.DATA, true);
                scriptText += MyMath.FormatBytesInBrackets(scriptAsBytes_.ToArray());

                scriptBytes = byteList.Concat(scriptAsBytes_).ToList();
                scriptBytesWithoutDelimiter = byteList.Concat(scriptBytesWithoutDelimiter_).ToList();

                break;
            }

            int i = ROM.DATA[pos];

            while (--i >= 0)
            {
                byteList.Add(ROM.DATA[pos++]);
            }
        }

        return scriptText;
    }

    private void BlankOutData(long startAddress, long byteSize, byte[] buffer)
    {
        if (Setting.settings.Misc.BlankOutTextDataAfterRead)
        {
            for (long i = startAddress; i < startAddress + byteSize; i++)
            {
                buffer[i] = Setting.settings.Misc.BlankOutByte;
            }
        }
    }
}