﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using static Enums;

public partial class TextParsing
{
    private readonly Regex ByteMatchRegex = new Regex(@"{.*?}");
    /*public string BytesToStringDictionaryTable(List<byte> Compressed, PointerReadWrite prw)
    {
        DictionaryTables[] DictionaryInteractionType = prw.RenderFormat.DictionaryInteractions;
        bool ReverseTableEndianness = prw.RenderFormat.ReverseTableEndianness;

        string DialogueLine = "";
        byte[] ByteData = Compressed.ToArray();
        int i = 0;
        int MaxByteLength = Math.Max(Chara.BiggestDictionaryKeyArraySize, Chara.BiggestCHRKeyArraySize);
        byte[] LookMeUp = null;
        while (i < ByteData.Length)
        {
            int LengthToTry = MaxByteLength;
            while (!(i + LengthToTry <= ByteData.Length))
            {
                LengthToTry--;
            }

            DictionaryTables Found = DictionaryTables.Null;

            while (Found == DictionaryTables.Null && LengthToTry > 0)
            {
                LookMeUp = new byte[LengthToTry];

                Array.Copy(ByteData, i, LookMeUp, 0, LengthToTry);

                if (ReverseTableEndianness)
                {
                    LookMeUp = LookMeUp.Reverse().ToArray();
                }

                // Control codes can override dictionary values. This is useful in cases such as Metal Slader Glory.
                // These checks are in place in case we disable the tables for an indextable for whatever reason.
                switch (DictionaryInteractionType)
                {
                    case DictionaryTables[] a when a.Contains(DictionaryTables.Dictionary):
                    case DictionaryTables[] b when b.Contains(DictionaryTables.ControlCode):
                    case DictionaryTables[] c when c.Contains(DictionaryTables.ChrDictionary):
                        if (Chara.DictControlcode.ContainsKey(LookMeUp))
                        {
                            Found = DictionaryTables.ControlCode;
                            goto Out;
                        }

                        if (Chara.DictSortByKey.ContainsKey(LookMeUp) && Chara.DictSortByKey[LookMeUp].Length != 0)
                        {
                            Found = DictionaryTables.Dictionary;
                            goto Out;
                        }
                        break;
                    default:
                        break;
                }

                if (DictionaryInteractionType.Contains(DictionaryTables.CHR) && Chara.IsCHRValid(LookMeUp))
                {
                    Found = DictionaryTables.CHR;
                    break;
                }

                LengthToTry--;
            }

            Out:

            if (LengthToTry == 0)
            {
                LengthToTry = 1;
            }

            i += LengthToTry;

            if (Found != DictionaryTables.Null)
            {
                string FoundDictionaryValue = "";
                int ArgumentLength = 0;

                switch (Found)
                {
                    case DictionaryTables.ControlCode:
                        FoundDictionaryValue = Chara.DictControlcode[LookMeUp].StringRepresentation;
                        ArgumentLength = Chara.DictControlcode[LookMeUp].CodeArgumentLength;
                        break;
                    case DictionaryTables.Dictionary:
                        FoundDictionaryValue = Chara.GetDictionaryAsString(Chara.DictSortByKey[LookMeUp], DictionaryInteractionType, out _);
                        break;
                    case DictionaryTables.CHR:
                        FoundDictionaryValue = Chara.GetCHRValue(LookMeUp);
                        break;
                    case DictionaryTables.Null:
                        throw new Exception();
                }

                if (string.IsNullOrEmpty(FoundDictionaryValue))
                {
                    throw new Exception("Found control code/dictionary/CHR value cannot be empty.");
                }

                DialogueLine += FoundDictionaryValue;

                //For Earthbound's "(Multiple-Address Reference Pointer MULTIPLIER)"
                if (FoundDictionaryValue.Contains("MULTIPLIER"))
                {
                    ArgumentLength *= ByteData[i];
                }

                while (ArgumentLength-- > Constants.CODE_ARGUMENT_LENGTH_NULL)
                {
                    if (ByteData.Length > i)
                    {
                        DialogueLine += MyMath.FormatByteInBrackets(ByteData[i++]);
                    }
                    else
                    {
                        throw new Exception("String ends to early. Control code expected an argument.");
                    }
                }
            }
            else
            {
                DialogueLine += MyMath.FormatByteInBrackets(LookMeUp);
            }
        }
        return DialogueLine;
    }*/

    public string BytesToStringKaettekitaMarioBrothers(List<byte> compressed, string key, PointerReadWrite prw)
    {
        // Make sure the first two bytes are printed as bytes in the string
        string Line = MyMath.FormatByteInBrackets(compressed[0]) + MyMath.FormatByteInBrackets(compressed[1]);
        compressed.RemoveAt(0);
        compressed.RemoveAt(0);
        return Line + ReadByteStreamWithPointer(0, key, prw, out _, out _, compressed.ToArray(), true);
    }

    public string BytesToStringMetalSladerNameCards(byte[] scriptAsBytes, string key, PointerReadWrite prw)
    {
        string scriptAsText = "";

        if (scriptAsBytes.Length > 1)
        {
            const byte METAL_SLADER_CODE = 0x00;

            if (scriptAsBytes[0] == METAL_SLADER_CODE)
            {
                //If 00 00 
                //Then it's just bytes of code.
                if (scriptAsBytes[1] == METAL_SLADER_CODE)
                {
                    foreach (byte b in scriptAsBytes)
                    {
                        scriptAsText += MyMath.FormatByteInBrackets(b);
                    }
                }
                //Otherwise, it's a namecard.
                else
                {
                    scriptAsText += MyMath.FormatByteInBrackets(scriptAsBytes[0]);
                    scriptAsText += MyMath.FormatByteInBrackets(scriptAsBytes[1]);

                    int NameCardSize = scriptAsBytes[1];
                    byte[] namecard = new byte[NameCardSize];
                    Array.Copy(scriptAsBytes, 2, namecard, 0, NameCardSize);

                    scriptAsText += ReadByteStreamWithPointer(0, key, prw, out _, out _, namecard, false, namecard.Length);
                        
                    for (int i = NameCardSize + 2; i < scriptAsBytes.Length; i++)
                    {
                        scriptAsText += MyMath.FormatByteInBrackets(scriptAsBytes[i]);
                    }
                }
            }
            else
            {
                scriptAsText += ReadByteStreamWithPointer(0, key, prw, out _, out _, scriptAsBytes, false, scriptAsBytes.Length);
            }
        }
        else
        {
            scriptAsText += ReadByteStreamWithPointer(0, key, prw, out _, out _, scriptAsBytes, false, scriptAsBytes.Length);
        }
        return scriptAsText;
    }

    public List<byte> ConvertScriptToHexWithDictionary(string line, long entryNo, PointerReadWrite prw, long pointerTableKey)
    {
        DictionaryTables[] DictionaryInteractionType = prw.RenderFormat.DictionaryInteractions;
        bool ReverseTableEndianness = prw.RenderFormat.ReverseTableEndianness;

        List<byte> Output = new List<byte>();
        Regex regex1 = new Regex(Constants.REGEX_SPLIT_STRING);

        string[] a_SubChunk = regex1.Split(line).Where(s => s != string.Empty).ToArray();

        Dictionary<string, byte[]> ControlCodeByString;
        Dictionary<string, byte[]> DictionaryByString;
        Dictionary<string, byte[]> CHRByString;

        int BiggestEntry;

        for (int i = 0; i < a_SubChunk.Length;)
        {
            BiggestEntry = -1;
            string Chunk = a_SubChunk[i];
            bool ValidChar = Chara.DictSortedByString.ContainsKey(Chunk) || Chara.CHRSortedByString.ContainsKey(Chunk);

            byte[] Add = null;

            // fix this

            if (ValidChar)
            {
                if ((DictionaryInteractionType.Contains(DictionaryTables.Dictionary) || DictionaryInteractionType.Contains(DictionaryTables.ChrDictionary) || DictionaryInteractionType.Contains(DictionaryTables.ControlCode)) && Chara.DictSortedByString.ContainsKey(Chunk))
                {
                    byte[] id = Chara.DictSortedByString[Chunk].ByKey.ElementAt(0).Key;
                    if (Chara.DictControlcode.ContainsKey(id))
                    {
                        ControlCodeByString = new Dictionary<string, byte[]> { { Chara.DictControlcode[id].StringRepresentation, id } };
                        BiggestEntry = 1;
                        DictionaryByString = new Dictionary<string, byte[]>();
                    }
                    else
                    {
                        ControlCodeByString = new Dictionary<string, byte[]>();
                        DictionaryByString = Chara.DictSortedByString[Chunk].ByString.OrderByDescending(x => x.Key.Length).ToDictionary(x => x.Key, x => x.Value);
                        BiggestEntry = Chara.DictSortedByString[Chunk].MaxNumberOfCharacters;
                    }
                }
                else
                {
                    ControlCodeByString = new Dictionary<string, byte[]>();
                    DictionaryByString = new Dictionary<string, byte[]>();
                }

                if (DictionaryInteractionType.Contains(DictionaryTables.CHR) && Chara.CHRSortedByString.ContainsKey(Chunk))
                {
                    CHRByString = Chara.CHRSortedByString[Chunk].ByString.OrderByDescending(x => x.Key.Length).ToDictionary(x => x.Key, x => x.Value);
                    BiggestEntry = Math.Max(Chara.CHRSortedByString[Chunk].MaxNumberOfCharacters, BiggestEntry);
                }
                else
                {
                    CHRByString = new Dictionary<string, byte[]>();
                }

                int LengthToTry = BiggestEntry;
                while (!(i + LengthToTry <= a_SubChunk.Length))
                {
                    LengthToTry--;
                }

                DictionaryTables Found = DictionaryTables.Null;
                string LookMeUp = "";
                while (Found == DictionaryTables.Null && LengthToTry > 0)
                {
                    string[] ss = new string[LengthToTry];
                    Array.Copy(a_SubChunk, i, ss, 0, LengthToTry);
                    LookMeUp = string.Join("", ss);

                    if (ControlCodeByString.ContainsKey(LookMeUp))
                    {
                        Found = DictionaryTables.ControlCode;
                        break;
                    }

                    if (DictionaryByString.ContainsKey(LookMeUp))
                    {
                        Found = DictionaryTables.Dictionary;
                        break;
                    }

                    if (DictionaryByString.ContainsKey(LookMeUp))
                    {
                        Found = DictionaryTables.Dictionary;
                        break;
                    }

                    if (CHRByString.ContainsKey(LookMeUp))
                    {
                        Found = DictionaryTables.CHR;
                        break;
                    }

                    LengthToTry--;
                }

                if (LengthToTry <= 0)
                {
                    throw new Exception($"Invalid length. Character to look for not found. Chunk: {Chunk}");
                }

                switch (Found)
                {
                    case DictionaryTables.ControlCode:
                        byte[] Id = ControlCodeByString[LookMeUp];
                        Add = Id;

                        bool EnoughArguments = true;
                        int ControlCodeArgumentLength = Chara.DictControlcode[Id].CodeArgumentLength;

                        if (i + ControlCodeArgumentLength < a_SubChunk.Length)
                        {
                            for(int j  = i + 1; j < i + 1+ ControlCodeArgumentLength; j++)
                            {
                                if (!ByteMatchRegex.IsMatch(a_SubChunk[j]))
                                {
                                    EnoughArguments = false;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            EnoughArguments = false;
                        }

                        if (!EnoughArguments)
                        {
                            throw new Exception($"Entry no {entryNo}'s control code \"{a_SubChunk[i]}\" didn't receive the amount of required argument bytes: {ControlCodeArgumentLength}. PointerTableKey: {MyMath.DecToHex(pointerTableKey, Prefix.X)}");
                        }

                        break;
                    case DictionaryTables.Dictionary:
                        byte[] Key = Chara.DictSortedByString[Chunk].ByString[LookMeUp];
                        Add = Key;
                        break;
                    case DictionaryTables.CHR:
                        Add = Chara.CHRSortedByString[Chunk].ByString[LookMeUp];
                        break;
                    default:
                        throw new Exception("Invalid dictionary table value.");
                }
                i += LengthToTry;
            }
            else if (Chunk.Contains("{"))
            {
                string result = (new Regex(@"[^A-Fa-f0-9]")).Replace(Chunk, string.Empty);

                if (result == string.Empty)
                {
                    throw new Exception($"Cannot insert script for entry #{entryNo}. Hex character malformed. PointerTableKey: {MyMath.DecToHex(pointerTableKey, Prefix.X)}");
                }

                int HexValue = MyMath.HexToDec(result);
                Add = new byte[] { (byte)HexValue };
                i++;
            }
            else
            {
                throw new Exception($"Entry no: {entryNo}. PointerTableKey: {MyMath.DecToHex(pointerTableKey, Prefix.X)}. Bad chunk \"{Chunk}\". Not in the tables in DictionaryInteractions.");
            }

            if (ReverseTableEndianness)
            {
                Add = Add.Reverse().ToArray();
            }

            Output = Output.Concat(Add).ToList();
        }

        return Output;
    }

    public List<byte> ConvertScriptToHexMarioPicrossSNES(string line, long entryNo, PointerReadWrite prw, long pointerTableKey)
    {
        /*
         * When dumping the Mario picross text we dump it as follows:
         * 
         * 1. Bunch of bytes (at end, is the puzzle name length)
         * 2. Puzzle name
         * 3. More bytes
         * 
         * Our goal is to extract the puzzle name, update the length, and update the length bytes in part 1.
         */

        if (line == "")
        {
            return new List<byte>();
        }

        string[] apuzzleName = (new Regex(@"({[A-Za-z0-9]{2}})+")).Split(line).Where(s => s != String.Empty).ToArray();

        List<string> puzzleBytes = new List<string>();

        foreach (Match m in (new Regex(@"({[A-Za-z0-9]{2}})+")).Matches(line))
        {
            if (m.Success)
            {
                puzzleBytes.Add(m.Value);
            }
        }

        if (puzzleBytes.Count != 2 && apuzzleName.Length != 3)
        {
            throw new Exception($"Unable to split string: entryNo: {entryNo}");
        }

        string puzzleName = apuzzleName[1];

        // puzzleName.Length is a lazy way to do this as characters in brackets count as a single character.

        // Calculate the puzzle name byte length
        int puzzleNameLength = ConvertScriptToHexWithDictionary(puzzleName, entryNo, prw, pointerTableKey).Count + 4;

        string puzzleBytes0Part1 = puzzleBytes[0].Substring(0, puzzleBytes[0].Length - 8);
        string formattedLength = MyMath.FormatByteInBrackets((byte)(puzzleNameLength & 0xFF)) + MyMath.FormatByteInBrackets((byte)(puzzleNameLength & 0xFF00));

        string newLine =
            puzzleBytes0Part1 +
             formattedLength +
             puzzleName +
            puzzleBytes[1];

        return ConvertScriptToHexWithDictionary(newLine, entryNo, prw, pointerTableKey);
    }
}