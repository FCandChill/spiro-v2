﻿using System;
using System.Linq;

partial class TextParsing
{
    public readonly MyDictionary Chara;

    public TextParsing()
    {
        Chara = new MyDictionary();
    }

    public void LoadNewTableFiles(bool readOriginal, string chrFile, string dictionaryFile, bool mirrorBlankDictionaryEntries, PointerReadWrite pointerReadWrite)
    {
        Chara.LoadTableFiles(readOriginal, chrFile, dictionaryFile, mirrorBlankDictionaryEntries);
    }

    public void AddValueToDictionary(byte[] addEntryToDictionary)
    {
        Chara.AddValueToDictionary(addEntryToDictionary);
    }
}