﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using static Enums;
using static ScriptIndexTable;

public partial class MainManager
{
    private readonly string settingsFilePath = "Settings.json";

    public MainManager(string projectFolder)
    {
        Directory.SetCurrentDirectory(projectFolder);
        Setting.ReadSettingsFile(settingsFilePath);
    }

    public void WriteScriptToROM()
    {
        List<string> writeToFileId = new List<string>();

        ROM.ReadRoms();

        // Read settings file.
        PointerManager PtrManager = new PointerManager();

        //Load in the CHR table, until we find the dictionary and load it in.
        TextParsing TxtIO = new TextParsing();

        ScriptDataStruct Scripts = new ScriptDataStruct();

        //read settings file.
        Setting.ReadSettingsFile(settingsFilePath);

        // Read script
        var Json = JsonConvert.DeserializeObject<ScriptIndexTable>(new StreamReader(Setting.settings.Scripts.ScriptPath).ReadToEnd());


        if (Global.Verbose)
        {
            Console.WriteLine($"Compressing text data.");
        }

        Stopwatch timer = Stopwatch.StartNew();

        WidthCalc LengthCalc = new WidthCalc();

        int byteSize = 0;
        int CharacterSize = 0;
        int bytesLost = 0;

        string currentChr = "";

        // Populate all the entries in Scripts data structure
        // This is for the "SameAs" field which can reference any text entry in the structure.
        foreach (KeyValuePair<string, ScriptIndexTable.Entry> ScriptEntry in Json.IndexTable)
        {
            if (!Setting.settings.Pointers.Write.ContainsKey(ScriptEntry.Key))
            {
                throw new KeyNotFoundException($"Key doesn't exist in settings file but is referenced in {Setting.settings.Scripts.New}: {ScriptEntry.Key}");
            }

            Scripts.IndexTables.Add(ScriptEntry.Key, new ScriptDataStruct.IndexTable());

            foreach (KeyValuePair<long, PointerTable> Entry in ScriptEntry.Value.PointerTable)
            {
                foreach (KeyValuePair<int, PointerTableEntry> PointerTableEntry in Entry.Value.PointerTableEntry)
                {
                    int LineNumber = PointerTableEntry.Key;
                    if (LineNumber >= Setting.settings.Pointers.Write[ScriptEntry.Key].PointerFormat[Constants.DefaultPointerIndex].EntryNumber)
                    {
                        throw new IndexOutOfRangeException($"Line key {LineNumber} out of range. Must be less than {Setting.settings.Pointers.Write[ScriptEntry.Key].PointerFormat[Constants.DefaultPointerIndex].EntryNumber}. Entry owner: {ScriptEntry.Key}; Address: {MyMath.DecToHex(Entry.Key, Prefix.X)}");
                    }

                    Scripts.IndexTables[ScriptEntry.Key].Entries.Add((Entry.Key, PointerTableEntry.Key), new ScriptDataStruct.Entry());
                    Scripts.IndexTables[ScriptEntry.Key].Entries[(Entry.Key, PointerTableEntry.Key)].SamePointerAs = PointerTableEntry.Value.SamePointerAs;
                }
            }
        }

        // Key1: Name of indextable
        foreach (KeyValuePair<string, ScriptIndexTable.Entry> sciptEntry in Json.IndexTable)
        {
            Console.WriteLine($"Encoding indextable: {sciptEntry.Key}");

            //CHR & Dictionary management start
            string newChrFile = Setting.settings.Pointers.Write[sciptEntry.Key].RenderFormat.CHRFile;
            string newDictionary = Setting.settings.Pointers.Write[sciptEntry.Key].RenderFormat.DictionaryFile;
            int pixelsPerLine = Setting.settings.Pointers.Write[sciptEntry.Key].RenderFormat.PixelsPerLine;
            bool isDictionary = Setting.settings.Pointers.Write[sciptEntry.Key].RenderFormat.IsDictionary;

            if (!string.IsNullOrEmpty(newChrFile))
            {
                string newChr = string.Format(Constants.Path_To_x_new, newChrFile);

                //If the CHR file changes.
                if (newChr != currentChr)
                {
                    //Update the file path for the CHR translation
                    currentChr = newChrFile;

                    // Check iwe're autoline breaking.
                    if (Setting.settings.Pointers.Write[sciptEntry.Key].RenderFormat.AutoLineBreak != AutoLineBreak.None)
                    {
                        //Load CHR data into length_calc
                        LengthCalc.LoadCHR(newChrFile);
                    }
                }
            }

            TxtIO.LoadNewTableFiles
            (
                readOriginal: false,
                chrFile: currentChr,
                dictionaryFile: Setting.settings.Pointers.Write[sciptEntry.Key].RenderFormat.DictionaryFile,
                mirrorBlankDictionaryEntries: Setting.settings.Pointers.Write[sciptEntry.Key].RenderFormat.MirrorBlankDictionaryEntries,
                pointerReadWrite: Setting.settings.Pointers.Write[sciptEntry.Key]
            );

            if (isDictionary)
            {
                LengthCalc.LoadDictionaryTemplate(newDictionary);
            }

            /*
             * String to byte stream
             */

            // Key1: Address of pointer table
            foreach (KeyValuePair<long, PointerTable> Entry in sciptEntry.Value.PointerTable)
            {
                // Key1: Line index
                foreach (KeyValuePair<int, PointerTableEntry> PointerTableEntry in Entry.Value.PointerTableEntry)
                {
                    if (Global.Debug)
                    {
                        Console.Out.WriteLine($"Pointer Table: {MyMath.DecToHex(Entry.Key, Prefix.X)}; Line number: {PointerTableEntry.Key}; ");
                    }

                    int LineNumber = PointerTableEntry.Key;

                    // See if a line points to the same line as another entry.
                    if (Setting.settings.Misc.MultiplePointersReferencingAString && PointerTableEntry.Value.SamePointerAs != null)
                    {
                        Scripts.IndexTables[sciptEntry.Key].Entries[(Entry.Key, PointerTableEntry.Key)].ScriptData = Scripts.IndexTables[PointerTableEntry.Value.SamePointerAs.IndexTable].Entries[(PointerTableEntry.Value.SamePointerAs.PointerTableLocation, PointerTableEntry.Value.SamePointerAs.EntryNo)].ScriptData;
                        continue;
                    }

                    string scriptAsText = PointerTableEntry.Value.Text[Enums.ScriptTypes.New][0];

                    //Line = Line.Replace("\"", "\\\"");

                    if (!string.IsNullOrEmpty(Setting.settings.Pointers.Write[sciptEntry.Key].RenderFormat.SquishyTextFile))
                    {
                        SquishyText squish = new SquishyText(
                            string.Format(Constants.Path_To_x_new,
                            Setting.settings.Pointers.Write[sciptEntry.Key].RenderFormat.SquishyTextFile),
                            Setting.settings.Pointers.Write[sciptEntry.Key].RenderFormat.SquishyTextFileUseregex
                            );
                        squish.ReadSquishyTextFile();
                        scriptAsText = squish.AddSquishyText(scriptAsText);
                    }

                    // Auto-linebreaking logic
                    if (!isDictionary)
                    {
                        switch (Setting.settings.Pointers.Write[sciptEntry.Key].RenderFormat.AutoLineBreak)
                        {
                            case Enums.AutoLineBreak.None:
                                break;
                            case Enums.AutoLineBreak.MetalSladerGlory:
                                scriptAsText = LengthCalc.MetalSladerTransAddLineBreaks(PointerTableEntry.Key, scriptAsText, pixelsPerLine, TxtIO.Chara);
                                break;
                            case Enums.AutoLineBreak.TokimekiMiho:
                                scriptAsText = LengthCalc.TokimekiMihoTransAddLineBreaks(PointerTableEntry.Key, scriptAsText, pixelsPerLine, TxtIO.Chara, true);
                                break;
                            case Enums.AutoLineBreak.TokimekiMihoMessage:
                                scriptAsText = LengthCalc.TokimekiMihoTransAddLineBreaks(PointerTableEntry.Key, scriptAsText, pixelsPerLine, TxtIO.Chara, false);
                                break;
                            default:
                                throw new Exception($"Auto line break. Invalid id: {Setting.settings.Pointers.Write[sciptEntry.Key].RenderFormat.AutoLineBreak}");
                        }
                    }

                    List<byte> ScriptAsBytes;

                    switch (Setting.settings.Pointers.Write[sciptEntry.Key].RenderFormat.RenderType)
                    {
                        case RenderType.Dictionary:
                        case RenderType.NoDictionaryMetalSladerNameCard:
                        case RenderType.KaettekitaMarioBrothersNagatanienWorldIntermission:
                            ScriptAsBytes = TxtIO.ConvertScriptToHexWithDictionary(scriptAsText, PointerTableEntry.Key,  Setting.settings.Pointers.Read[sciptEntry.Key], Entry.Key);
                            break;
                        case RenderType.MarioPicrossSNES:
                            ScriptAsBytes = TxtIO.ConvertScriptToHexMarioPicrossSNES(scriptAsText, PointerTableEntry.Key, Setting.settings.Pointers.Read[sciptEntry.Key], Entry.Key);
                            break;
                        default:
                            throw new Exception($"Invalid render type: {Setting.settings.Pointers.Write[sciptEntry.Key].RenderFormat.RenderType}");
                    }

                    Scripts.IndexTables[sciptEntry.Key].Entries[(Entry.Key, PointerTableEntry.Key)].ScriptData = ScriptAsBytes.ToArray();

                    if (isDictionary && Setting.settings.Pointers.Write[sciptEntry.Key].RenderFormat.AutoLineBreak != Enums.AutoLineBreak.None)
                    {
                        LengthCalc.AddDictionaryEntryLength(PointerTableEntry.Value.Text[Enums.ScriptTypes.New][0], TxtIO.Chara);
                    }

                    byteSize += ScriptAsBytes.Count;
                    CharacterSize += PointerTableEntry.Value.Text[Enums.ScriptTypes.New][0].Length;

                    if (isDictionary)
                    {
                        TxtIO.AddValueToDictionary(ScriptAsBytes.ToArray());
                    }
                }
            }
        }

        timer.Stop();

        if (Global.Verbose)
        {
            Console.WriteLine($"Compressed text to bytes in: {timer.ElapsedMilliseconds / 1000} second(s).");
            Console.WriteLine($"Compressed script size: {MyMath.DecToHex(byteSize, Prefix.X)} bytes.");
            Console.WriteLine($"Uncompressed script size: {MyMath.DecToHex(CharacterSize, Prefix.X)} characters.");
        }

        /*
         * We just finished converting the script to byte data. Now let's prepare to
         * write to the ROM.
         */

        Dictionary<string, Dictionary<long, object[]>> Pointers = new Dictionary<string, Dictionary<long, object[]>>();
        Dictionary<string, List<byte>> ByteData = new Dictionary<string, List<byte>>();

        foreach (string IndexTableName in Setting.settings.Pointers.Write.Keys)
        {
            Pointers.Add(IndexTableName, new Dictionary<long, object[]>());

            foreach (long Addr in Setting.settings.Pointers.Write[IndexTableName].PointerFormat[Constants.DefaultPointerIndex].Address)
            {
                Pointers[IndexTableName].Add(Addr, new object[Setting.settings.Pointers.Write[IndexTableName].PointerFormat[Constants.DefaultPointerIndex].EntryNumber]);
            }
        }

        foreach (string k in Setting.settings.Write.WriteableRange.Keys)
        {
            ByteData.Add(k, new List<byte>());
        }

        if (Global.Verbose)
        {
            Console.WriteLine($"Creating addresses and arranged data.");
        }

        timer = Stopwatch.StartNew();

        foreach (WriteRegion WriteRegionEntry in Setting.settings.Write.WriteRegion)
        {
            Enums.E_DataType DataType = E_DataType.NoDataTpye;

            int writeRegionI = 0;

            foreach (string EntryOwner in WriteRegionEntry.EntryOwners)
            {
                if (!Setting.settings.Pointers.Write.ContainsKey(EntryOwner))
                {
                    throw new Exception($"Invalid WriteRegion entry owner: {EntryOwner}");
                }

                Stack<KeyValuePair<(long, int), byte[]>> linesSortedBySize = new Stack<KeyValuePair<(long, int), byte[]>>();
                bool storeOutOfOrderToSaveSpace = Setting.settings.Pointers.Write[EntryOwner].PointerFormat[Constants.DefaultPointerIndex].StoreOutOfOrderToSaveSpace;

                // Setup LinesSortedBySize
                {
                    if (!Scripts.IndexTables.ContainsKey(EntryOwner))
                    {
                        throw new Exception($"The translated script doesn't contain the entry owner \"{EntryOwner}\"");
                    }

                    Console.WriteLine($"Writing entry owner: {EntryOwner}.");

                    if (Global.Debug && storeOutOfOrderToSaveSpace)
                    {
                        Console.WriteLine("Space saving mode activated.");
                        Console.WriteLine("Lines will be written out of order from biggest to smallest.");

                        SortedList<int, KeyValuePair<(long, int), byte[]>> Linesbysize = new SortedList<int, KeyValuePair<(long, int), byte[]>>(new DuplicateKeyComparer<int>());

                        foreach (KeyValuePair<(long, long), ScriptDataStruct.Entry> e in Scripts.IndexTables[EntryOwner].Entries)
                        {
                            Linesbysize.Add(e.Value.ScriptData.Length, new KeyValuePair<(long, int), byte[]>((e.Key.Item1, (int)e.Key.Item2), e.Value.ScriptData));
                        }

                        foreach (KeyValuePair<int, KeyValuePair<(long, int), byte[]>> kvp in Linesbysize)
                        {
                            linesSortedBySize.Push(kvp.Value);
                        }
                    }
                    else
                    {
                        var Temp = Scripts.IndexTables[EntryOwner].Entries.ToList();
                        Temp.Reverse();

                        foreach (KeyValuePair<(long, long), ScriptDataStruct.Entry> e in Temp)
                        {
                            linesSortedBySize.Push(new KeyValuePair<(long, int), byte[]>((e.Key.Item1, (int)e.Key.Item2), e.Value.ScriptData));
                        }
                    }
                }

                while (linesSortedBySize.Count != 0)
                {
                    KeyValuePair<(long, int), byte[]> kvp = linesSortedBySize.Pop();
                    long Address = kvp.Key.Item1;
                    int LineId = kvp.Key.Item2;

                    byte[] scriptAsBytes = kvp.Value;

                    if (LineId >= Pointers[EntryOwner][Address].Length)
                    {
                        throw new IndexOutOfRangeException($"Line key {LineId} out of range. Must be less than {Pointers[EntryOwner][Address].Length}. Entry owner: {EntryOwner}; Address: {MyMath.DecToHex(Address, Prefix.X)}");
                    }

                    if (Global.Debug)
                    {
                        Console.WriteLine($"Line number: {LineId}");
                    }

                    /*
                     * The WriteRegion_i is the slot number in XML.
                     * It's not the actual value.
                     * 
                     * Enter the loop if we've run out of space in the write region. 
                     * This is a loop in case the next write region is not big enough 
                     * for some reason.
                     */

                    string Region2Write2 = "";

                    if (storeOutOfOrderToSaveSpace)
                    {
                        writeRegionI = 0;
                    }

                    while (true)
                    {
                        int LineLength = (scriptAsBytes == null ? 0 : scriptAsBytes.Length);

                        string Range = WriteRegionEntry.WriteableAddressRanges[writeRegionI];

                        if (!ByteData.ContainsKey(Range))
                        {
                            throw new Exception($"Invalid WriteableAddressRange: {Range}");
                        }
                        int WriteRegionLength = ByteData[Range].Count;

                        // Check if need to migrate to the next available space slot.
                        if (!(Setting.settings.Write.WriteableRange[WriteRegionEntry.WriteableAddressRanges[writeRegionI]].Size < LineLength + WriteRegionLength))
                        {
                            break;
                        }

                        // When StoreOutOfOrderToSaveSpace is set to true, this can get really noisy, so reserve it for debug.
                        if ((!storeOutOfOrderToSaveSpace && Global.Verbose) || (storeOutOfOrderToSaveSpace && Global.Debug))
                        {
                            Console.WriteLine($"Writable region name: {writeRegionI}");
                            Console.WriteLine($"Free space in region: {MyMath.DecToHex(bytesLost += scriptAsBytes.Length, Prefix.X)}");
                        }

                        if (++writeRegionI >= WriteRegionEntry.WriteableAddressRanges.Length)
                        {
                            long freeSpace = Setting.settings.Write.WriteableRange[WriteRegionEntry.WriteableAddressRanges[writeRegionI - 1]].Size - ByteData[WriteRegionEntry.WriteableAddressRanges[writeRegionI - 1]].Count;
                            int linesLeftToInsert = linesSortedBySize.Count + 1;
                            int uninsertedDataSize = 0;
                            uninsertedDataSize += kvp.Value.Length;
                            while (linesSortedBySize.Count != 0)
                            {
                                uninsertedDataSize += linesSortedBySize.Pop().Value.Length;
                            }

                            throw new Exception(
                                $"Not enough space.\n" +
                                $"Entry owner name:        {EntryOwner}\n" +
                                $"Line number:             {LineId}/{Scripts.IndexTables[EntryOwner].Entries.Count - 1}\n" +
                                $"Lines left:              {linesLeftToInsert}\n" +
                                $"Write subregion Size:    0x{Setting.settings.Write.WriteableRange[WriteRegionEntry.WriteableAddressRanges[writeRegionI - 1]].Size:X4} bytes\n" +
                                $"Size of uninserted data: 0x{uninsertedDataSize:X4} bytes\n" +
                                $"Free space in region:    0x{freeSpace:X4} bytes\n" +
                                $"Space overflow:          0x{uninsertedDataSize - freeSpace:X4} bytes\n"
                                );
                        }

                        if ((!storeOutOfOrderToSaveSpace && Global.Verbose) || (storeOutOfOrderToSaveSpace && Global.Debug))
                        {
                            Console.WriteLine($"Write region: {writeRegionI}");
                        }
                    }

                    Region2Write2 = WriteRegionEntry.WriteableAddressRanges[writeRegionI];

                    /*
                     * UsePointerFromID is for when two entries have the same text and use the same pointer.
                     * We want the inserter to make sure the pointers match when updating the pointers.
                     */
                    if (Scripts.IndexTables[EntryOwner].Entries[(Address, LineId)].SamePointerAs == null)
                    {
                        long PcAddressOfPlaceToWrite = ByteData[Region2Write2].Count + Setting.settings.Write.WriteableRange[Region2Write2].StartAddress;
                        long ConvertedAddress;
                        byte[] ByteArray = null;

                        bool IsValidPcAddress = true;

                        // Convert address
                        switch (Setting.settings.Pointers.Write[EntryOwner].PointerFormat[Constants.DefaultPointerIndex].AddressConversion)
                        {
                            case Enums.AddressConversion.None:
                                ConvertedAddress = PcAddressOfPlaceToWrite;
                                break;
                            case Enums.AddressConversion.LoROM1:
                                ConvertedAddress = AddressLoROM1.PcToSnes((int)PcAddressOfPlaceToWrite, out IsValidPcAddress);
                                break;
                            case Enums.AddressConversion.LoROM2:
                                ConvertedAddress = AddressLoROM2.PcToSnes((int)PcAddressOfPlaceToWrite, out IsValidPcAddress);
                                break;
                            case Enums.AddressConversion.HiROM:
                                ConvertedAddress = AddressHiRom.PcToSnes((int)PcAddressOfPlaceToWrite, out IsValidPcAddress);
                                break;
                            case Enums.AddressConversion.ExLoROM:
                                ConvertedAddress = AddressExLoROM.PcToSnes((int)PcAddressOfPlaceToWrite, out IsValidPcAddress);
                                break;
                            case Enums.AddressConversion.ExHiROM:
                                ConvertedAddress = AddressExHiROM.PcToSnes((int)PcAddressOfPlaceToWrite, out IsValidPcAddress);
                                break;
                            default:
                                throw new Exception();
                        }

                        if (!IsValidPcAddress)
                        {
                            throw new Exception($"Address conversion failed. Not a valid PC address: 0x{PcAddressOfPlaceToWrite:X8}");
                        }

                        long Diff = Setting.settings.Pointers.Write[EntryOwner].PointerFormat[Constants.DefaultPointerIndex].PcDifference;

                        if (ConvertedAddress - Diff < 0)
                        {
                            throw new Exception($"Address negative after subtraction. {ConvertedAddress} - {Diff} = {ConvertedAddress + Diff}");
                        }

                        ConvertedAddress -= Diff;

                        switch (Setting.settings.Pointers.Write[EntryOwner].PointerFormat[Constants.DefaultPointerIndex].PointerType)
                        {
                            case Enums.PointerType.LittleEndian:
                                ByteArray = PtrManager.CreatePointerLittleEndian(ConvertedAddress, Setting.settings.Pointers.Write[EntryOwner].PointerFormat[Constants.DefaultPointerIndex].PointerLength);
                                DataType = E_DataType.ByteArray;
                                break;
                            case Enums.PointerType.ThreeByteMetalSladerDXPrimaryNoLength:
                                /*
                                 * Bits for this address are zeroed out when you're using a delimiter,
                                 * so we pass the length as zero here.
                                 */
                                ConvertedAddress = PtrManager.CreateAddressMetalSladerSNES(ConvertedAddress, 0);
                                DataType = E_DataType.IntAddress;
                                break;
                            case Enums.PointerType.ThreeByteMetalSladerDXPrimary:
                                ConvertedAddress = PtrManager.CreateAddressMetalSladerSNES(ConvertedAddress, scriptAsBytes.Length);
                                DataType = E_DataType.IntAddress;
                                break;
                            case Enums.PointerType.ThreeByteMetalSladerNESPrimaryNoLength:
                                ConvertedAddress = PtrManager.CreateAddressMetalSladerNES(ConvertedAddress, 0);
                                DataType = E_DataType.IntAddress;
                                break;
                            case Enums.PointerType.ThreeByteMetalSladerNESPrimary:
                                ConvertedAddress = PtrManager.CreateAddressMetalSladerNES(ConvertedAddress, scriptAsBytes.Length);
                                DataType = E_DataType.IntAddress;
                                break;
                            case Enums.PointerType.ThreeByteMetalSladerNESTransPrimaryNoLength:
                                ConvertedAddress = PtrManager.CreateAddressMetalSladerNES(ConvertedAddress, 0, true);
                                DataType = E_DataType.IntAddress;
                                break;
                            case Enums.PointerType.None:
                                DataType = E_DataType.NoDataTpye;
                                break;
                            case Enums.PointerType.Custom:
                                ByteArray = PtrManager.CreateAddressCustomFormat(ConvertedAddress, Setting.settings.Pointers.Write[EntryOwner].PointerFormat[Constants.DefaultPointerIndex].CustomPointerFormat);
                                DataType = E_DataType.ByteArray;
                                break;
                            default:
                                throw new Exception();
                        }

                        switch (DataType)
                        {
                            case E_DataType.NoDataTpye:
                                break;
                            case E_DataType.IntAddress:
                                Pointers[EntryOwner][Address][LineId] = ConvertedAddress;
                                break;
                            case E_DataType.ByteArray:
                                Pointers[EntryOwner][Address][LineId] = ByteArray;
                                break;
                            default:
                                throw new Exception($"No pointer data type. Pointer type: {Setting.settings.Pointers.Write[EntryOwner].PointerFormat[Constants.DefaultPointerIndex].PointerType}.");
                        }

                        ByteData[Region2Write2] = ByteData[Region2Write2].Concat(scriptAsBytes).ToList();
                    }
                    else
                    {
                        SamePointerAs SameAs = Scripts.IndexTables[EntryOwner].Entries[(Address, LineId)].SamePointerAs;

                        // Add some sanity check bullshit for the "SamePointerAs" struct here.

                        Pointers[EntryOwner][Address][LineId] = Pointers[SameAs.IndexTable][SameAs.PointerTableLocation][SameAs.EntryNo];
                    }
                }
            }

            if (WriteRegionEntry.EntryOwners.Length == 0)
            {
                throw new Exception("Can't have zero EntryOwners");
            }

            if (WriteRegionEntry.WriteableAddressRanges.Length == 0)
            {
                throw new Exception("Can't have zero WriteableAddressRanges");
            }

            //now write the pointers
            foreach (string EntryKey in WriteRegionEntry.EntryOwners)
            {
                foreach (long Address in Setting.settings.Pointers.Write[EntryKey].PointerFormat[Constants.DefaultPointerIndex].Address)
                {
                    long PointerLength = Setting.settings.Pointers.Write[EntryKey].PointerFormat[Constants.DefaultPointerIndex].PointerLength;
                    object[] PointerTable = Pointers[EntryKey][Address];

                    if (Setting.settings.Misc.GetFileToWritePointerFromPointersField)
                    {
                        ROM.CURRENT_ROM = Setting.settings.Pointers.Write[EntryKey].PointerFormat[Constants.DefaultPointerIndex].File;
                    }
                    else
                    {
                        ROM.CURRENT_ROM = Setting.settings.Write.WriteableRange[WriteRegionEntry.WriteableAddressRanges[0]].File;
                    }

                    if (!writeToFileId.Contains(ROM.CURRENT_ROM))
                    {
                        writeToFileId.Add(ROM.CURRENT_ROM);
                    }

                    if (ROM.DATA == null)
                    {
                        throw new NullReferenceException($"Current rom doesn't exist: id: {ROM.CURRENT_ROM}; Filepath: {Setting.settings.Files[ROM.CURRENT_ROM]}");
                    }

                    long ByteBetween = Setting.settings.Pointers.Write[EntryKey].PointerFormat[Constants.DefaultPointerIndex].BytesBetween;

                    switch (DataType)
                    {
                        case E_DataType.NoDataTpye:
                            break;
                        case E_DataType.IntAddress:
                            for (int i = 0; i < PointerTable.Length; i++)
                            {
                                /*
                                 * We have to manipulate the bitconverter output array because it 
                                 * a) Reversed the byte order.
                                 * b) Convert it to a X length Pointer[Constants.DefaultPointerIndex]. Bitconverter outputs 4 bytes (an int),
                                 *    so we need to cut it as needed.
                                 */

                                if (PointerTable[i] == null)
                                {
                                    throw new Exception($"Entry #{i} doesn't exist in settings file.");
                                }

                                byte[] PointerAsBytes = BitConverter.GetBytes(Convert.ToUInt32(PointerTable[i])).Reverse().ToArray();
                                byte[] Truncated = new byte[Setting.settings.Pointers.Write[EntryKey].PointerFormat[Constants.DefaultPointerIndex].PointerLength];
                                Array.Copy(PointerAsBytes, PointerAsBytes.Length - Truncated.Length, Truncated, 0, Truncated.Length);
                                Array.Copy(Truncated, 0, ROM.DATA, (int)Address + PointerLength * i + (i * ByteBetween), PointerLength);
                            }

                            break;
                        case E_DataType.ByteArray:
                            for (int i = 0; i < PointerTable.Length; i++)
                            {
                                byte[] b = (byte[])PointerTable[i];

                                if (b == null)
                                {
                                    throw new Exception($"Pointer is null. Index {i}");
                                }

                                if (b.Length == 0)
                                {
                                    throw new Exception("Pointer array cannot be empty");
                                }

                                long PointerLocation = Address + PointerLength * i + (i * ByteBetween);

                                if (PointerLocation > ROM.DATA.Length)
                                {
                                    throw new Exception($"Pointer location out of bounds for file, \"{Setting.settings.Files[ROM.CURRENT_ROM]}\". Pointer location: {MyMath.DecToHex(PointerLocation, Prefix.X)}; File size: {MyMath.DecToHex(ROM.DATA.Length, Prefix.X)}");
                                }

                                Array.Copy(b, 0, ROM.DATA, PointerLocation, PointerLength);
                            }
                            break;
                    }
                }
            }
        }

        timer.Stop();

        if (Global.Verbose)
        {
            Console.WriteLine($"Created addresses and arranged data in: {timer.ElapsedMilliseconds / 1000} second(s).");
        }

        int FreespaceCount = 0;
        int WastedSpaceCount = 0;

        //Write region to the ROM
        foreach (KeyValuePair<string, WriteableRange> Kvp in Setting.settings.Write.WriteableRange)
        {
            //fill the data with zeros

            int initialspace = ByteData[Kvp.Key].Count;

            while (Setting.settings.Write.WriteableRange[Kvp.Key].Size > ByteData[Kvp.Key].Count)
            {
                ByteData[Kvp.Key].Add(Constants.FREESPACE_byte);
                FreespaceCount++;
                
                // Don't count regions that haven't been written to as having wasted space.
                if (initialspace != 0)
                {
                    WastedSpaceCount++;
                }
            }

            ROM.CURRENT_ROM = Setting.settings.Write.WriteableRange[Kvp.Key].File;

            if (!writeToFileId.Contains(ROM.CURRENT_ROM))
            {
                writeToFileId.Add(ROM.CURRENT_ROM);
            }

            if (Setting.settings.Write.WriteableRange[Kvp.Key].Size != ByteData[Kvp.Key].Count)
            {
                throw new Exception();
            }

            if (ROM.DATA.Length < (int)Setting.settings.Write.WriteableRange[Kvp.Key].StartAddress)
            {
                throw new Exception($"ROM address to write to, 0x{Setting.settings.Write.WriteableRange[Kvp.Key].StartAddress:X2}, is out of bounds.");
            }

            if (ROM.DATA.Length < (int)Setting.settings.Write.WriteableRange[Kvp.Key].StartAddress + Setting.settings.Write.WriteableRange[Kvp.Key].Size)
            {
                throw new Exception($"Write size too big for ROM: ROM: {ROM.CURRENT_ROM}; ROM Length: {MyMath.DecToHex(ROM.DATA.Length, Prefix.X)}; StartAddress: {MyMath.DecToHex(Setting.settings.Write.WriteableRange[Kvp.Key].StartAddress, Prefix.X)}; Size: {MyMath.DecToHex(Setting.settings.Write.WriteableRange[Kvp.Key].Size, Prefix.X)}");
            }

            //add data to writable regions
            Array.Copy(ByteData[Kvp.Key].ToArray(), 0, ROM.DATA, (int)Setting.settings.Write.WriteableRange[Kvp.Key].StartAddress, Setting.settings.Write.WriteableRange[Kvp.Key].Size);
        }

        foreach (string ss in writeToFileId)
        {
            File.WriteAllBytes(Setting.settings.Files[ss], ROM.ALL[ss]);
        }

        if (Global.Verbose)
        {
            Console.WriteLine($"Free space:   {MyMath.DecToHex(FreespaceCount, Prefix.X)}");
            Console.WriteLine($"Wasted space: {MyMath.DecToHex(WastedSpaceCount, Prefix.X)}");
        }
    }
}