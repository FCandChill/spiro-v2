﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using static Enums;

partial class MainManager
{
    public void DumpScript()
    {
        List<string> writeToFileId = new List<string>();

        ROM.ReadRoms();

        //read settings file.
        Setting.ReadSettingsFile(settingsFilePath);

        PointerManager PtrReader = new PointerManager();

        // Key 1: Address
        // Key 2: Entry No
        SortedDictionary<string, SortedDictionary<(long, int), Entry>> entries = new SortedDictionary<string, SortedDictionary<(long, int), Entry>>();

        // Key 1: Text file-id
        // Key 2: Pointer Address:
        // Value: First associated EntryNo
        // Used to determnine if multiple pointers reference the same string
        SortedDictionary<(string, long), SamePointerAs> pointerUsage = new SortedDictionary<(string, long), SamePointerAs>();

        if (Global.Verbose)
        {
            Console.Out.WriteLine($"Getting pointers.");
        }

        foreach (KeyValuePair<string, PointerReadWrite> readKvp in Setting.settings.Pointers.Read)
        {
            if (Global.Verbose)
            {
                Console.Out.WriteLine($"Reading pointers for {readKvp.Key}");
            }

            ROM.CURRENT_ROM = readKvp.Value.PointerFormat[0].File;

            if (!writeToFileId.Contains(ROM.CURRENT_ROM) && Setting.settings.Misc.BlankOutTextDataAfterRead)
            {
                writeToFileId.Add(ROM.CURRENT_ROM);
            }

            if (ROM.DATA == null)
            {
                throw new Exception($"Having trouble reading file: {Setting.settings.Files[ROM.CURRENT_ROM]}");
            }

            if (readKvp.Value.PointerFormat[Constants.DefaultPointerIndex].CustomPointerFormat != null && (readKvp.Value.PointerFormat[Constants.DefaultPointerIndex].PointerType == PointerType.Custom ^ readKvp.Value.PointerFormat[Constants.DefaultPointerIndex].CustomPointerFormat.Length != 0))
            {
                throw new Exception($"PointerType and SpecialPointerFormat both must be filled out for a custom pointer to be read.");
            }

            entries.Add(readKvp.Key, new SortedDictionary<(long, int), Entry>());

            foreach (long pointerTableLocation in readKvp.Value.PointerFormat[Constants.DefaultPointerIndex].Address)
            {
                for (int entryNo = 0; entryNo < readKvp.Value.PointerFormat[Constants.DefaultPointerIndex].EntryNumber; entryNo++)
                {
                    if (entries[readKvp.Key].ContainsKey((pointerTableLocation, entryNo)))
                    {
                        throw new Exception($"A text entry with the following information already exists: pointerTableLocation: {MyMath.DecToHex(pointerTableLocation, Prefix.X)}, entryNo: {entryNo}");
                    }

                    entries[readKvp.Key].Add((pointerTableLocation, entryNo), new Entry());

                    var pointerType = readKvp.Value.PointerFormat[Constants.DefaultPointerIndex].PointerType;
                    var pointerGrouping = readKvp.Value.PointerFormat[Constants.DefaultPointerIndex].PointerGrouping;
                    long pointerLength = readKvp.Value.PointerFormat[Constants.DefaultPointerIndex].PointerLength;

                    long pointerAddress = -1;
                    long pointerLocation = -1;
                    long scriptLength = readKvp.Value.RenderFormat.FixedSize;

                    bool hasPointer = true;

                    switch (pointerGrouping)
                    {
                        case Enums.PointerGrouping.Normal:
                            if (pointerType == Enums.PointerType.None)
                            {
                                throw new Exception("Can't have the normal pointer grouping when there isn't a pointer.");
                            }

                            pointerLocation = pointerTableLocation + (entryNo * pointerLength) + entryNo * readKvp.Value.PointerFormat[Constants.DefaultPointerIndex].BytesBetween;
                            break;
                        case Enums.PointerGrouping.SingleEntryFixedSize:
                        case Enums.PointerGrouping.Single:

                            if (entryNo > 0)
                            {
                                hasPointer = false;
                            }

                            pointerLocation = pointerTableLocation;
                            break;
                        default:
                            throw new Exception($"Invalid pointer grouping vakue: {pointerGrouping}");
                    }

                    if (pointerLocation > ROM.DATA.Length)
                    {
                        throw new Exception($"Pointer Location out of bounds: Region {readKvp.Key}; ROM: {ROM.CURRENT_ROM}; ROM Length: 0x{MyMath.DecToHex(ROM.DATA.Length)}; PointerTableLocation: 0x{MyMath.DecToHex(pointerTableLocation)}; PointerLocation: 0x{MyMath.DecToHex(pointerLocation)}");
                    }

                    entries[readKvp.Key][(pointerTableLocation, entryNo)].MasterPointerLocations = pointerLocation;

                    if (pointerLength < 0)
                    {
                        throw new Exception("Cannot have a pointer lenth less than zero.");
                    }

                    byte[] pointerBytes = new byte[pointerLength];

                    if (pointerBytes.Length != 0)
                    {
                        Array.Copy(ROM.DATA, (int)pointerLocation, pointerBytes, 0, pointerBytes.Length);
                    }

                    if (Global.Debug)
                    {
                        Console.WriteLine($"Reading address: {MyMath.DecToHex(pointerLocation, Prefix.X)}; Name: {readKvp.Key};Index: {entryNo:0000};");
                    }

                    bool NoPointer = false;
                    switch (pointerType)
                    {
                        case Enums.PointerType.LittleEndian:
                            PtrReader.ReadLittleEndian(pointerBytes, out pointerAddress);
                            break;
                        case Enums.PointerType.ThreeByteMetalSladerDXPrimaryNoLength:
                        case Enums.PointerType.ThreeByteMetalSladerDXPrimary:
                            PtrReader.GetAddressMetalSladerSNES(pointerBytes, out pointerAddress, out scriptLength);
                            break;
                        case Enums.PointerType.ThreeByteMetalSladerNESPrimaryNoLength:
                        case Enums.PointerType.ThreeByteMetalSladerNESPrimary:
                            PtrReader.GetAddressMetalSladerNES(pointerBytes, out pointerAddress, out scriptLength);
                            break;
                        case Enums.PointerType.ThreeByteMetalSladerNESTransPrimaryNoLength:
                            PtrReader.GetAddressMetalSladerNES(pointerBytes, out pointerAddress, out scriptLength, true);
                            break;
                        case Enums.PointerType.BigEndian:
                            PtrReader.ReadBigEndian(pointerBytes, out pointerAddress);
                            break;
                        case Enums.PointerType.None:
                            NoPointer = true;
                            pointerAddress = pointerTableLocation;
                            break;
                        case Enums.PointerType.Custom:
                            string[] customPointerFormat = readKvp.Value.PointerFormat[Constants.DefaultPointerIndex].CustomPointerFormat;
                            int entryPointerLength = readKvp.Value.PointerFormat[Constants.DefaultPointerIndex].PointerLength;

                            if (customPointerFormat == null)
                            {
                                throw new Exception("customPointerFormat is null");
                            }

                            if (customPointerFormat.Length != entryPointerLength)
                            {
                                throw new Exception($"customPointerFormat.Length and pointerLength must be the same: {customPointerFormat.Length} != {entryPointerLength}");
                            }

                            PtrReader.GetAddressCustomFormat(pointerBytes, customPointerFormat, out pointerAddress);
                            break;
                        default:
                            throw new Exception($"Invalid pointertype number: {pointerType}");
                    }

                    switch (pointerGrouping)
                    {
                        case Enums.PointerGrouping.Normal:
                            break;
                        case Enums.PointerGrouping.Single:
                            break;
                        case Enums.PointerGrouping.SingleEntryFixedSize:
                            pointerAddress += entryNo * scriptLength;
                            break;
                        default:
                            throw new Exception($"Invalid pointer grouping vakue: {pointerGrouping}");
                    }

                    if (!NoPointer)
                    {
                        // Handle PC difference logic before address conversion logic
                        // This gives the opportunity to adjust the pointer to the correct bank, for example.
                        long pcDiff = readKvp.Value.PointerFormat[Constants.DefaultPointerIndex].PcDifference;

                        if (pointerAddress + pcDiff < 0)
                        {
                            throw new Exception($"Address negative after subtraction. {MyMath.DecToHex(pointerAddress, Prefix.X)} - {MyMath.DecToHex(Math.Abs(pcDiff), Prefix.X)} = -{MyMath.DecToHex(Math.Abs(pointerAddress + pcDiff), Prefix.X)}");
                        }

                        pointerAddress = pcDiff + pointerAddress;

                        bool isValidPcAddress = true;

                        switch (readKvp.Value.PointerFormat[Constants.DefaultPointerIndex].AddressConversion)
                        {
                            case Enums.AddressConversion.None:
                                break;
                            case Enums.AddressConversion.LoROM1:
                                pointerAddress = AddressLoROM1.SnesToPc((int)pointerAddress, out isValidPcAddress);
                                break;
                            case Enums.AddressConversion.LoROM2:
                                pointerAddress = AddressLoROM2.SnesToPc((int)pointerAddress, out isValidPcAddress);
                                break;
                            case Enums.AddressConversion.HiROM:
                                pointerAddress = AddressHiRom.SnesToPc((int)pointerAddress, out isValidPcAddress);
                                break;
                            case Enums.AddressConversion.ExLoROM:
                                pointerAddress = AddressExLoROM.SnesToPc((int)pointerAddress, out isValidPcAddress);
                                break;
                            case Enums.AddressConversion.ExHiROM:
                                pointerAddress = AddressExHiROM.SnesToPc((int)pointerAddress, out isValidPcAddress);
                                break;
                            default:
                                throw new Exception($"Invalid address conversion: {readKvp.Value.PointerFormat[Constants.DefaultPointerIndex].AddressConversion}");
                        }

                        if (!isValidPcAddress)
                        {
                            throw new Exception("Address conversion failed.");
                        }

                        if (!ROM.ALL.ContainsKey(readKvp.Value.RenderFormat.File))
                        {
                            throw new KeyNotFoundException($"The following key from \"{readKvp.Key}\"'s RenderFormat does not exist in Files: {readKvp.Value.RenderFormat.File}");
                        }

                        if ((int)pointerAddress > ROM.ALL[readKvp.Value.RenderFormat.File].Length)
                        {
                            throw new IndexOutOfRangeException($"Pointer bigger than ROM size. Key: {readKvp.Key}; EntryNo: {entryNo}; Pointer Table Location: {MyMath.DecToHex(pointerTableLocation, Prefix.X)}; PointerAddress: {MyMath.DecToHex(pointerAddress)}");
                        }
                    }

                    if (Global.Debug)
                    {
                        Console.WriteLine($"Bytes: {MyMath.FormatByteInBrackets(pointerBytes)}; Pointer Address: 0x{pointerAddress:X8}; Length: 0x{scriptLength:X3}");
                    }

                    // AddressOfPointer, EntryNo
                    entries[readKvp.Key][(pointerTableLocation, entryNo)].MasterPointerAddresses = pointerAddress;
                    entries[readKvp.Key][(pointerTableLocation, entryNo)].MasterLength = scriptLength;



                    /*
                     * If pointeraddress is not in the PointerUsage dictionary,
                     * add the location of the pointer to it.
                     * 
                     * This will be used later to determine if another pointer entry references the same string.
                     * If so, it will be documented in the script dump and then used when inserting.
                     */
                    if (hasPointer)
                    {
                        string fileKey = readKvp.Value.RenderFormat.File;
                        if (!pointerUsage.ContainsKey((fileKey, pointerAddress)))
                        {
                            pointerUsage.Add((fileKey, pointerAddress), new SamePointerAs(readKvp.Key, pointerTableLocation, entryNo));
                        }
                    }

                    entries[readKvp.Key][(pointerTableLocation, entryNo)].MasterLength = scriptLength;
                }
            }
        }

        File.Delete(Setting.settings.Scripts.ScriptPath);

        TextParsing txtIO = new TextParsing();
        ScriptIndexTable dumpedScript = new ScriptIndexTable();

        if (Global.Verbose)
        {
            Console.Out.WriteLine($"Reading data");
        }

        Stopwatch timer = Stopwatch.StartNew();

        foreach (KeyValuePair<string, PointerReadWrite> kvp in Setting.settings.Pointers.Read)
        {
            txtIO.LoadNewTableFiles
            (
                readOriginal: true,
                chrFile: kvp.Value.RenderFormat.CHRFile,
                dictionaryFile: kvp.Value.RenderFormat.DictionaryFile,
                mirrorBlankDictionaryEntries: kvp.Value.RenderFormat.MirrorBlankDictionaryEntries,
                pointerReadWrite: kvp.Value
            );

            dumpedScript.IndexTable.Add(kvp.Key, new ScriptIndexTable.Entry());

            if (Global.Verbose)
            {
                Console.Out.WriteLine($"Getting data for: {kvp.Key}");
            }

            foreach (KeyValuePair<(long, int), Entry> EntryKvp in entries[kvp.Key])
            {
                var pointerGrouping = kvp.Value.PointerFormat[Constants.DefaultPointerIndex].PointerGrouping;
                int entryNo = EntryKvp.Key.Item2;

                List<byte> scriptBytes;
                List<byte> scriptBytesWithoutDelimiter = new List<byte>();
                string scriptAsText = "";

                if (Global.Debug)
                {
                    Console.Out.WriteLine($"Index table: {kvp.Key}; PointerTable:{MyMath.DecToHex(EntryKvp.Key.Item1, Prefix.X)};");
                }

                if (!dumpedScript.IndexTable[kvp.Key].PointerTable.ContainsKey(EntryKvp.Key.Item1))
                {
                    dumpedScript.IndexTable[kvp.Key].PointerTable.Add(EntryKvp.Key.Item1, new ScriptIndexTable.PointerTable());
                }

                ROM.CURRENT_ROM = kvp.Value.RenderFormat.File;
                switch (kvp.Value.RenderFormat.DialogueReadType)
                {
                    case (int)Enums.DialogueReadType.HasDelimiter:
                        // Originally "HasDelimiter" and "Dictionary" had their own functions.
                        // However, since the logic was extremely similar, they were merged into one function.
                        scriptAsText = txtIO.ReadByteStreamWithPointer(EntryKvp.Value.MasterPointerAddresses, kvp.Key, kvp.Value, out scriptBytes, out scriptBytesWithoutDelimiter, ROM.DATA, true);
                        break;
                    case Enums.DialogueReadType.HasLengthVar:
                        scriptBytesWithoutDelimiter = scriptBytes = txtIO.ReadByteStreamWithPointerInSize(entries[kvp.Key][(EntryKvp.Key.Item1, entryNo)], ROM.DATA);
                        break;
                    case Enums.DialogueReadType.MarioPicrossSNES:
                        scriptAsText = txtIO.ReadByteMarioPicrossSNES(EntryKvp.Value.MasterPointerAddresses, kvp.Key, kvp.Value, out scriptBytes, out scriptBytesWithoutDelimiter);
                        break;
                    default:
                        throw new Exception();
                }

                bool hasPointer = true;
                bool cascadingEntries = false;

                switch (pointerGrouping)
                {
                    case Enums.PointerGrouping.Normal:
                        break;
                    case Enums.PointerGrouping.SingleEntryFixedSize:
                    case Enums.PointerGrouping.Single:
                        cascadingEntries = true;

                        if (entryNo > 0)
                        {
                            hasPointer = false;
                        }

                        break;
                    default:
                        throw new Exception($"Invalid pointer grouping vakue: {pointerGrouping}");
                }

                // If each entry is one after the other, adjust the next entry's pointer accordingly.
                if (cascadingEntries && entries.ContainsKey(kvp.Key) && entries[kvp.Key].ContainsKey((EntryKvp.Key.Item1, entryNo)))
                {
                    var nextKey = (EntryKvp.Key.Item1, entryNo + 1);
                    if (entries[kvp.Key].ContainsKey(nextKey))
                    {
                        entries[kvp.Key][nextKey].MasterPointerAddresses = EntryKvp.Value.MasterPointerAddresses + scriptBytes.Count;
                    }
                }

                if (Global.Debug)
                {
                    Console.Out.WriteLine($"Primary: { MyMath.DecToHex(entries[kvp.Key][(EntryKvp.Key.Item1, entryNo)].MasterPointerAddresses, Prefix.X)}:");
                }

                if (Global.Debug)
                {
                    Console.Out.WriteLine(MyMath.FormatBytesInBrackets(scriptBytes.ToArray()));
                }

                //Load dictionary if it's the first entry. Modify text file.
                if (kvp.Value.RenderFormat.IsDictionary)
                {
                    txtIO.AddValueToDictionary(scriptBytesWithoutDelimiter.ToArray());
                }

                switch (kvp.Value.RenderFormat.RenderType)
                {
                    //Byte => text with dictionary table.
                    case RenderType.Dictionary:
                        if (scriptAsText != "")
                        {
                            scriptAsText = txtIO.ReadByteStreamWithPointer(0, kvp.Key, kvp.Value, out _, out _, scriptBytes.ToArray(), false, scriptBytes.Count);
                        }
                        break;
                    case RenderType.NoDictionaryMetalSladerNameCard:
                        scriptAsText = txtIO.BytesToStringMetalSladerNameCards(scriptBytes.ToArray(), kvp.Key, kvp.Value);
                        break;
                    case RenderType.KaettekitaMarioBrothersNagatanienWorldIntermission:
                        scriptAsText = txtIO.BytesToStringKaettekitaMarioBrothers(scriptBytes, kvp.Key, kvp.Value);
                        break;
                    case RenderType.MarioPicrossSNES:
                        //scriptAsText = txtIO.ConvertToStringMarioPicrossSNES(EntryKvp.Value, scriptBytes, kvp.Value);
                        break;
                    default:
                        throw new Exception($"Invalid render type: {kvp.Value.RenderFormat.RenderType}");
                }

                if (Global.Debug)
                {
                    Console.WriteLine($"Decompressed Line: {scriptAsText}");
                }

                if (!string.IsNullOrEmpty(kvp.Value.RenderFormat.SquishyTextFile))
                {
                    SquishyText squish = new SquishyText(string.Format(Constants.Path_To_x_orig, kvp.Value.RenderFormat.SquishyTextFile), kvp.Value.RenderFormat.SquishyTextFileUseregex);
                    squish.ReadSquishyTextFile();
                    scriptAsText = squish.AddSquishyText(scriptAsText);
                }

                long pointerLocation = entries[kvp.Key][(EntryKvp.Key.Item1, entryNo)].MasterPointerLocations;
                long pointerAddress = entries[kvp.Key][(EntryKvp.Key.Item1, entryNo)].MasterPointerAddresses;
                SamePointerAs sameAs = null;

                if (hasPointer)
                {
                    sameAs = pointerUsage[(kvp.Value.RenderFormat.File, pointerAddress)];
                }

                // If we found an entry in PointerUsage, make sure it's not the current line
                if (Setting.settings.Misc.MultiplePointersReferencingAString && sameAs != null && !sameAs.Equals(IndexTable: kvp.Key, PointerTableLocation: EntryKvp.Key.Item1, EntryNo: entryNo))
                {
                    scriptAsText = null;
                }
                else
                {
                    sameAs = null;
                }

                Dictionary<Enums.ScriptTypes, List<string>> TextEntries = new Dictionary<Enums.ScriptTypes, List<string>>();

                if (Setting.settings.Scripts.Original)
                {
                    TextEntries.Add(Enums.ScriptTypes.Original, new List<string>() { scriptAsText });
                }

                if (Setting.settings.Scripts.New)
                {
                    TextEntries.Add(Enums.ScriptTypes.New, new List<string>() { scriptAsText == null ? null : "" });
                }

                if (Setting.settings.Scripts.Comment)
                {
                    TextEntries.Add(Enums.ScriptTypes.Comment, new List<string>() { scriptAsText == null ? null : "" });
                }

                if (Setting.settings.Scripts.Proof)
                {
                    TextEntries.Add(Enums.ScriptTypes.Proof, new List<string>() { scriptAsText == null ? null : "" });
                }

                if (Setting.settings.Scripts.Bytes)
                {
                    TextEntries.Add(Enums.ScriptTypes.Bytes, new List<string>() { MyMath.FormatBytesInBrackets(scriptBytes.ToArray()) });
                }

                if (Setting.settings.Scripts.Menu)
                {
                    TextEntries.Add(Enums.ScriptTypes.Menu, new List<string>() { "" });
                }

                dumpedScript.IndexTable[kvp.Key].PointerTable[EntryKvp.Key.Item1].PointerTableEntry.Add(EntryKvp.Key.Item2, new ScriptIndexTable.PointerTableEntry()
                {
                    PointerLocation = MyMath.DecToHex(pointerLocation, Prefix.X),
                    TextLocation = MyMath.DecToHex(pointerAddress, Prefix.X),
                    ByteLength = MyMath.DecToHex(scriptBytes.Count, Prefix.X),
                    Text = TextEntries,
                    SamePointerAs = sameAs
                }
                );
            }

            if (kvp.Value.RenderFormat.IsDictionary)
            {
                txtIO.LoadNewTableFiles
                (
                    readOriginal: true,
                    chrFile: kvp.Value.RenderFormat.CHRFile,
                    dictionaryFile: kvp.Value.RenderFormat.DictionaryFile,
                    mirrorBlankDictionaryEntries: kvp.Value.RenderFormat.MirrorBlankDictionaryEntries,
                    pointerReadWrite: kvp.Value
                );
            }
        }

        timer.Stop();

        if (Global.Verbose)
        {
            Console.WriteLine($"Dumped script text in: {timer.ElapsedMilliseconds / 1000} second(s).");
        }

        if (!Directory.Exists(Path.GetDirectoryName(Setting.settings.Scripts.ScriptPath)))
        {
            throw new Exception($"Path for script doesn't exist: {Path.GetFullPath(Path.GetDirectoryName(Setting.settings.Scripts.ScriptPath))}");
        }

        File.WriteAllText(Setting.settings.Scripts.ScriptPath, JsonConvert.SerializeObject(dumpedScript, Formatting.Indented));

        foreach (string ss in writeToFileId)
        {
            /*
             * The "BlankOutTextDataAfterRead" is used to figure out what regions of the rom store a script.
             * The rom is modified to do so, however, we don't want to modify the original roms, so ".blanked"
             * is added to the extension.
             */
            File.WriteAllBytes(Path.ChangeExtension(Setting.settings.Files[ss], Path.GetExtension(Setting.settings.Files[ss]) + ".blanked"), ROM.ALL[ss]);
        }
    }
}