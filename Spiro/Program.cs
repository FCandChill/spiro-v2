﻿using System;
using System.Linq;

namespace Spiro
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
#if DEBUG
#else
            try
            {
#endif
                if (args.Length >= 2)
                {
                    string projectDirectory = "";

                    int index = Array.IndexOf(args, @"/ProjectDirectory");

                    if (index >= 0 && index + 1 <= args.Length)
                    {
                        projectDirectory = args[index + 1];
                    }
                    else
                    {
                        Console.WriteLine("Please specify a \"ProjectDirectory\" argument and the path directly after.");
                        System.Environment.Exit(1);
                    }

                    if (args.Any("/Verbose".Contains))
                    {
                        Global.Verbose = true;
                    }

                    if (args.Any("/Debug".Contains))
                    {
                        Global.Debug = true;
                    }

                    MainManager m = new MainManager(projectDirectory);

                    if (args.Any(new string[] { "/WriteScriptToROM", "/Write" }.Contains))
                    {
                        m.WriteScriptToROM();
                        Console.WriteLine("Inserted script successfully.");
                    }
                    else if (args.Any(new string[] { "/DumpScript", "/Dump", "/Read" }.Contains))
                    {
                        m.DumpScript();
                        Console.WriteLine("Dumped script to JSON file.");
                    }
                    else
                    {
                        Console.WriteLine("Please specify on whether to dump or insert a script.");
                        PrintUsage();
                        System.Environment.Exit(1);
                    }

                    Console.WriteLine("Finished successfully.");
                }
#if DEBUG
#else
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message.ToString());
            }
            finally
            {
#endif

#if DEBUG
#else
            }
#endif

        }

        private static void PrintUsage()
        {
            Console.WriteLine("USAGE:");
            Console.WriteLine("    File.exe:");
            Console.WriteLine("        /ProjectDirectory $PATH");
            Console.WriteLine("        [/WriteScriptToROM | /ReadScript]");
            Console.WriteLine("        [/Verbose]");
            Console.WriteLine("        [/Debug]");

            Console.WriteLine("    Options:");
            Console.WriteLine("       /WriteScriptToROM     Write the script to the ROM file. File name is from your XML cypro file.");
            Console.WriteLine("       /DumpScript           Dump script to XML file.");
            Console.WriteLine("       /Verbose              Verbose output.");
        }
    }
}